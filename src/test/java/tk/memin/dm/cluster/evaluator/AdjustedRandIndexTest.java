package tk.memin.dm.cluster.evaluator;

import static org.junit.Assert.assertEquals;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class AdjustedRandIndexTest
{
	@Test
	public void Computes_ARI_Between_Two_Partitions()
	{
		int[] part_1 = new int[]
		{ 1, 1, 1, 2, 2 };
		int[] part_2 = new int[]
		{ 1, 1, 2, 2, 2 };

		double e = 0.1666;
		double a = AdjustedRandIndex.between(c(part_1), c(part_2));
		assertEquals(e, a, 0.0001);
	}

	@Test
	public void Computes_ARI_Between_Two_Partitions_2()
	{
		int[] part_1 = new int[]
		{ 1, 1, 2, 2, 2, 3, 3, 3, 3, 3 };
		int[] part_2 = new int[]
		{ 1, 2, 1, 2, 2, 2, 3, 3, 3, 3 };

		double e = 0.31257;
		double a = AdjustedRandIndex.between(c(part_1), c(part_2));
		assertEquals(e, a, 0.00001);
	}

	private static BitSet[] c(int[] parts)
	{
		Map<Integer, BitSet> bitsets = new HashMap<Integer, BitSet>();
		for (int i = 0; i < parts.length; i++)
		{
			BitSet bs = bitsets.get(parts[i]);
			if (bs == null)
			{
				bs = new BitSet();
				bitsets.put(parts[i], bs);
			}
			bs.set(i);
		}
		return bitsets.values().toArray(new BitSet[0]);
	}
}
