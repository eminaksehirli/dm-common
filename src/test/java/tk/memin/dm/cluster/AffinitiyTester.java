package tk.memin.dm.cluster;

import static java.lang.Double.parseDouble;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class AffinitiyTester
{
	public static void main(String[] args) throws FileNotFoundException
	{
		double[][] points = preparePoints();

		double[][] similarityMatrix = calculateSimilarityMatrix(points);

		AffinityTools.putMedianToDiagonals(similarityMatrix);
		// AffinityTools.putZeroToDiagonals(similarityMatrix);
		// AffinityTools.printTheMatrixForMatlab(similarityMatrix);

		AffinitityFrame frame = new AffinitityFrame(points);
		frame.showYourself();

		AffinityPropagation algorithm = new AffinityPropagation(similarityMatrix);
		// algorithm.runTheAlgorithm(10);
		while (true)
		{
			algorithm.runOneIteration();
			int[] labels = algorithm.findExemplars();
			frame.showLabels(labels);

			for (int label : labels)
			{
				System.out.print(label + ",");
			}
			System.out.println();

			try
			{
				Thread.sleep(2000);
			} catch (InterruptedException e)
			{
			}
		}
	}

	private static double[][] preparePoints() throws FileNotFoundException
	{
		List<String[]> pointList = new LinkedList<String[]>();
		// Scanner sc = new Scanner(new File("data/gaussian.csv"));
		Scanner sc = new Scanner(new File("data/toy.csv"));
		// String path = getClass().getResource("/resources/toy.csv").getPath();

		// Scanner sc = new Scanner(new File(path));
		while (sc.hasNext())
		{
			String line = sc.nextLine();
			if (line.length() == 0)
			{
				continue;
			}
			String[] pointStr = line.split(";");

			pointList.add(pointStr);
		}

		int numOfObjects = pointList.size();

		double[][] points = new double[numOfObjects][];

		int counter = 0;
		for (String[] pointStr : pointList)
		{
			points[counter++] = new double[]
			{ parseDouble(pointStr[0]), parseDouble(pointStr[1]) };

		}
		return points;
	}

	private static double[][] calculateSimilarityMatrix(double[][] points)
	{
		double[][] similarityMatrix = new double[points.length][points.length];

		for (int i = 0; i < points.length; i++)
		{
			for (int j = i + 1; j < points.length; j++)
			{
				similarityMatrix[i][j] = similarityMatrix[j][i] = 1 / gaussianDistanceBetween(
						points[i], points[j]);
			}
		}
		return similarityMatrix;
	}

	private static double gaussianDistanceBetween(double[] p, double[] p2)
	{
		double sum = 0.0;

		for (int dimension = 0; dimension < p.length; dimension++)
		{
			double diff = p[dimension] - p2[dimension];
			sum += diff * diff;
		}

		return Math.sqrt(sum);
	}

	@SuppressWarnings("unused")
	private static void printTheMatrixForMatlab(double[][] similarityMatrix)
	{
		for (int i = 0; i < similarityMatrix.length; i++)
		{
			double[] row = similarityMatrix[i];
			StringBuilder rowStr = new StringBuilder();
			for (int j = 0; j < row.length; j++)
			{
				double cell = row[j];
				rowStr.append(cell).append(",");
			}
			System.out.println(rowStr.substring(0, rowStr.length() - 1) + ";");
		}
	}
}
