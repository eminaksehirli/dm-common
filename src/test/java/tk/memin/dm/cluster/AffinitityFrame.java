package tk.memin.dm.cluster;

import javax.swing.JFrame;

public class AffinitityFrame extends JFrame
{
	private static final long serialVersionUID = -3256497990336364722L;
	private PointsDrawingPanel panel;

	public AffinitityFrame(double[][] points)
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		panel = new PointsDrawingPanel(points);
		add(panel);
	}

	public void showYourself()
	{
		setSize(500, 500);
		setVisible(true);
	}

	void showLabels(int[] labels)
	{
		panel.setLabels(labels);
	}

}
