package tk.memin.dm.cluster;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class PointsDrawingPanel extends JPanel
{
	private static final long serialVersionUID = 7749228235044429539L;
	private static final int Scale = 70;
	private static final int Ofset = 50;
	private int[][] pointRepresentations;
	private int[] labels;

	public PointsDrawingPanel(double[][] points)
	{
		pointRepresentations = new int[points.length][];
		for (int i = 0; i < points.length; i++)
		{
			double[] p = points[i];
			pointRepresentations[i] = new int[]
			{ convert(p[0]), convert(p[1]) };
		}
	}

	@Override
	protected void paintComponent(Graphics g1)
	{
		Graphics2D g = (Graphics2D) g1;
		super.paintComponent(g);
		for (int i = 0; i < pointRepresentations.length; i++)
		{
			int[] p = pointRepresentations[i];
//			g.fillOval(p[0], p[1], 5, 5);
			g.drawString(i+"", p[0], p[1]);
		}

		for (int i = 0; i < labels.length; i++)
		{
			int[] from = pointRepresentations[i];
			int[] to = pointRepresentations[labels[i]];
			int mid_x = (from[0] + to[0]) / 2;
			int mid_y = (from[1] + to[1]) / 2;
			g.setColor(Color.green);
			g.drawLine(from[0], from[1], mid_x, mid_y);
			g.setColor(Color.black);
			g.drawLine(mid_x, mid_y, to[0], to[1]);
		}
	}

	private static int convert(double p_0)
	{
		return (int) (p_0 * Scale) + Ofset;
	}

	public int[][] points()
	{
		return pointRepresentations;
	}

	public void setLabels(int[] labels)
	{
		this.labels = labels;
		repaint();
	}
}
