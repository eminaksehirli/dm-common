package tk.memin.dm.cluster;

import static java.lang.Double.valueOf;
import tk.memin.dm.cluster.kmeans.KMeans;
import tk.memin.dm.io.EasyFileReader;

public class KMeansTest
{
	public static void main(String[] args)
	{
		EasyFileReader reader = new EasyFileReader("data/input2D.txt");
		double[][] values = new double[100][2];

		int lineCounter = 0;
		for (String line : reader)
		{
			String[] valueStrings = line.split(";");
			values[lineCounter][0] = valueOf(valueStrings[0]);
			values[lineCounter][1] = valueOf(valueStrings[1]);
			lineCounter++;
		}

		KMeans algorithm = KMeans.kMeansDefault();
		int[] labels = algorithm.run(values, 3);

		System.out.println("Labels: ");
		for (int label : labels)
		{
			System.out.print((label + 1) + ",");
		}
	}
}
