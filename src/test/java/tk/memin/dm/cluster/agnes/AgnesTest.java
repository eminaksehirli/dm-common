package tk.memin.dm.cluster.agnes;

import java.util.HashSet;
import java.util.List;

import tk.memin.dm.cluster.agnes.Agnes;
import tk.memin.dm.cluster.agnes.AgnesMergerFactory;

public class AgnesTest
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		double[][] matrix = new double[][]
		{
		{ 9, 3, 6, 7, 1 },
		{ 3, 9, 5, 1, 3 },
		{ 6, 5, 9, 2, 5 },
		{ 7, 1, 2, 9, 4 },
		{ 1, 3, 5, 4, 9 } };

		Agnes agnes = new Agnes(matrix,
				AgnesMergerFactory.averageLink());
		agnes.run(3);

		List<HashSet<Integer>> clusters = agnes.clusters();

		for (HashSet<Integer> cluster : clusters)
		{
			System.out.println(cluster);
		}

		/*
		 * [4] [0, 3] [1, 2] -> for CL and AL _____________________________________
		 * [1] [4] [0, 2, 3] -> for SL
		 */
	}
}
