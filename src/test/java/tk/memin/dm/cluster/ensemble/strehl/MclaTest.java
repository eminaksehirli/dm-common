package tk.memin.dm.cluster.ensemble.strehl;

import java.util.BitSet;

import tk.memin.dm.cluster.ensemble.ExtractClusterings;
import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;
import tk.memin.dm.cluster.ensemble.strehl.Mcla;

public class MclaTest
{
	public static void main(String[] args)
	{
		ClusterInfo clusterInfo = ExtractClusterings.fromFile("data/strehl-source.txt");
		Mcla algorithm = new Mcla(clusterInfo);

		algorithm.run(3);

		for (BitSet cluster : algorithm.finalClusters)
		{
			for (int i = cluster.nextSetBit(0); i >= 0; i = cluster.nextSetBit(i + 1))
			{
				System.out.print(i + ",");
			}
			System.out.println();
		}
	}
}
