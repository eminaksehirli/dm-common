package tk.memin.dm.cluster.ensemble;

import org.junit.Test;

import tk.memin.dm.cluster.ensemble.MatrixHelper;

public class AssociationMatrixTest
{
	@Test
	public void simple_Test()
	{
		String path = getClass().getResource("/input.csv").getPath();
		int[][] matrix = MatrixHelper.associationFrom(path);

		for (int[] row : matrix)
		{
			for (int cell : row)
			{
				System.out.print(String.format("%4d", cell));
			}
			System.out.println();
		}
	}
}
