package tk.memin.dm.cluster.ensemble.strehl;

import java.util.List;

import tk.memin.dm.cluster.ensemble.ExtractClusterings;
import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;
import tk.memin.dm.cluster.ensemble.strehl.Cspa;

public class CspaTest
{
	public static void main(String[] args)
	{
		ClusterInfo clusterInfo = ExtractClusterings.fromFile("data/strehl-source.txt");
		Cspa algorithm = new Cspa(clusterInfo);

		algorithm.run(3);

		for (List<Integer> cluster : algorithm.finalClusters)
		{
			for (int object : cluster)
			{
				System.out.print(object + ",");
			}
			System.out.println();
		}

	}
}
