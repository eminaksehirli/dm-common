package tk.memin.dm.text;

import static org.junit.Assert.*;

import org.junit.Test;

import tk.memin.dm.text.TextProcessor;

public class TextProcessorTest
{
	@Test
	public void removesNonChars()
	{
		String aString = "asd@f a'qwe bR( ee )44fs, b Afd.";

		String actual = TextProcessor.removeNonChars(aString);

		assertEquals("asd f a qwe bR ee 44fs b Afd", actual);
	}

	@Test
	public void removesStopWords()
	{
		String aString = "you'll fidelity then you";
//		String aString = "fidelity";

		String actual = TextProcessor.removeStopWords(aString);

		assertEquals("fidelity", actual);
	}

}
