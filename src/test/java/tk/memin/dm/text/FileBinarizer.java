package tk.memin.dm.text;

import tk.memin.dm.binary.DocumentBinarizerForMimarogluData;
import static java.lang.Integer.parseInt;

public class FileBinarizer
{
	private static final String INFO = "info";
	private static final String BITSET = "bitset";

	public static void main(String[] args)
	{
		String mode = args[0];
		String filename = args[1];

		if (INFO.startsWith(mode))
		{
			DocumentBinarizerForMimarogluData.extractCountsFrom(filename);
		} else if (BITSET.startsWith(mode))
		{
			int numOfLines = parseInt(args[2]);
			int numOfWords = parseInt(args[3]);
			DocumentBinarizerForMimarogluData.printAsBinary(filename, numOfLines, numOfWords);
		}
	}
}
