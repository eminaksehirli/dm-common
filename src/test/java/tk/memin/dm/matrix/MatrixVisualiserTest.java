package tk.memin.dm.matrix;

import org.junit.Test;

public class MatrixVisualiserTest
{
	public static void main(String[] args)
	{
		// showFrameTest();
		show_Frame_Is_Gray_Scale();
	}

	public static void showFrameTest()
	{
		double[][] matrix = new double[][]
		{
		{ 1, 82, 130, 184, 250 },
		{ 1, 82, 130, 184, 250 },
		{ 1, 82, 130, 184, 250 },
		{ 1, 82, 130, 184, 250 },
		{ 1, 82, 130, 184, 250 } };
		MatrixVisualiser.showFrame(matrix);
	}

	private static void show_Frame_Is_Gray_Scale()
	{
		double[][] matrix = new double[300][300];

		for (int i = 0; i < matrix.length; i++)
		{
			for (int j = 0; j < matrix[i].length; j++)
			{
				matrix[i][j] = 2 * j;
			}
		}
		MatrixVisualiser.showFrame(matrix);
	}

	@Test
	public void top_K_Tester()
	{
		double[][] matrix = new double[][]
		{
		{ 1.0, 2.3, 3.4, 2.78, 9.2 },
		{ 4, 2.3, 15, 2.78, 2.5 },
		{ 1.0, 2.3, 13, 12.78, 9.2 }

		};

		MatrixVisualiser.printRowMaxes(matrix, 2);
	}
}
