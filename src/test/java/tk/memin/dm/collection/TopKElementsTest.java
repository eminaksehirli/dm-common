package tk.memin.dm.collection;

import static java.lang.Integer.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import tk.memin.dm.collection.TopKElements;

public class TopKElementsTest
{
	@Test
	public void finds_Top_3_Elements()
	{
		double[] values = new double[]
		{ 1.3, 3.4, 1.2, 5, 6, 7.3 };

		List<Integer> tops = TopKElements.of(values, 3);

		assertTrue(tops.remove(valueOf(4))); // 7.3
		assertTrue(tops.remove(valueOf(5))); // 6
		assertTrue(tops.remove(valueOf(3))); // 5

		assertEquals(0, tops.size());
	}
}
