package tk.memin.dm.collection;

import java.util.LinkedList;
import java.util.List;

public class TopKElements
{
	public static List<Integer> of(double[] array, int k)
	{
		LinkedList<Double> rowMaxes = new LinkedList<Double>();
		LinkedList<Integer> rowMaxIxs = new LinkedList<Integer>();

		for (int j = 0; j < k; j++)
		{
			rowMaxes.add(0.0);
			rowMaxIxs.add(0);
		}

		for (int j = 0; j < array.length; ++j)
		{
			double element = array[j];
			int guaranteedPosition = -1;
			int position = 0;
			for (Double rowMax : rowMaxes)
			{
				if (element > rowMax)
				{
					guaranteedPosition = position;
				} else
				{
					break;
				}
				position++;
			}
			if (guaranteedPosition != -1)
			{
				rowMaxes.pop();
				rowMaxIxs.pop();

				rowMaxes.add(guaranteedPosition, element);
				rowMaxIxs.add(guaranteedPosition, j);
			}
		}
		return rowMaxIxs;
	}

}
