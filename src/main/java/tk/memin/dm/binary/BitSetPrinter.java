package tk.memin.dm.binary;

import java.util.BitSet;

public class BitSetPrinter
{
	public static String asRawString(BitSet bs)
	{
		return asRawString(bs, bs.size());
	}

	/**
	 * Converts a bitset to a string of 1s and 0s without any delimiters.
	 * 
	 * @param bs
	 *          bitset to print
	 * @param bitsToConsider
	 *          number of bits to print
	 * @return For the bitset of {1, 0, 0, 1, 1} it returns "10011".
	 */
	public static String asRawString(BitSet bs, int bitsToConsider)
	{
		StringBuilder builder = new StringBuilder();
		for (int ix = 0; ix < bitsToConsider; ix++)
		{
			if (bs.get(ix))
			{
				builder.append("1");
			} else
			{
				builder.append("0");
			}
		}
		return builder.toString();
	}

	/**
	 * Converts a bitset to a string of 1s and 0s delimited with a comma.
	 * 
	 * @param bs
	 *          bitset to print
	 * @return For the bitset of {1, 0, 0, 1, 1} it returns "1,0,0,1,1".
	 */
	public static String asCsvString(BitSet bs)
	{
		StringBuilder builder = new StringBuilder();
		for (int ix = 0; ix < bs.size(); ix++)
		{
			if (bs.get(ix))
			{
				builder.append("1,");
			} else
			{
				builder.append("0,");
			}
		}
		return builder.substring(0, builder.length() - 1);
	}
}
