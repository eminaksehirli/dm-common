package tk.memin.dm.binary;

import tk.memin.dm.io.EasyFileReader;

/**
 * This class reads a binary dataset and computes some very basic statistics
 * about it.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class BinaryDataSetExaminerRaw
{
	private int numberOfLines;
	private int numberOfOnes;
	private int maxSize;
	private int maxNumberOfOnes;
	private int minNumberOfOnes;
	private double sparsity;
	private int total;

	public static BinaryDataSetExaminerRaw runOn(String filename)
	{
		return new BinaryDataSetExaminerRaw(filename);
	}

	public BinaryDataSetExaminerRaw(String filename)
	{
		EasyFileReader reader = new EasyFileReader(filename);

		numberOfLines = 0;
		numberOfOnes = 0;
		maxSize = 0;
		maxNumberOfOnes = 0;
		minNumberOfOnes = Integer.MAX_VALUE;
		sparsity = 0;
		total = 0;
		int onesForThisLine;

		for (String line : reader)
		{
			++numberOfLines;
			onesForThisLine = 0;
			// String[] chars = line.split(",");

			if (line.length() > maxSize)
			{
				maxSize = line.length();
			}

			for (int i = 0; i < line.length(); ++i)
			{
				total++;
				if (line.charAt(i) == '1')
				{
					onesForThisLine++;
				}
			}

			numberOfOnes += onesForThisLine;

			if (onesForThisLine > maxNumberOfOnes)
			{
				maxNumberOfOnes = onesForThisLine;
			}
			if (onesForThisLine < minNumberOfOnes)
			{
				minNumberOfOnes = onesForThisLine;
				System.err.println("min lines: " + numberOfLines);
			}
		}

		sparsity = (double) numberOfOnes / (double) total;
	}

	@Override
	public String toString()
	{
		return "BinaryDataSetExaminerCsv \n[total=" + total + "\n maxNumberOfOnes="
				+ maxNumberOfOnes + "\n minNumberOfOnes=" + minNumberOfOnes
				+ "\n maxSize=" + maxSize + "\n numberOfLines=" + numberOfLines
				+ "\n numberOfOnes=" + numberOfOnes + "\n sparsity=" + sparsity + "]";
	}
}
