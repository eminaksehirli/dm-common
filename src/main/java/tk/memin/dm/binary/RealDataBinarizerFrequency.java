package tk.memin.dm.binary;

import static java.lang.Double.parseDouble;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import tk.memin.dm.io.EasyFileReader;

/**
 * This class binarizes a sub-matrix of a data set using ranking.
 * <code>numOfBitsPerAttr</code> number of bits are used for each
 * <code>numOfAttr</code> number of attributes. Each data is assigned to a bit
 * by determining the place of data in a sorted list of column values.
 * 
 * @deprecated Experimental class, refactor and clean it if you have to use it
 */
@Deprecated
public class RealDataBinarizerFrequency
{
	static Random r = new Random();

	public static void main(String args[])
	{
		String filename = "data/segmentation.test";
		int numOfAttr = 7;
		int ofset = 10;
		int numOfBitsPerAttr = 6;
		int numOfRows = 2100;

		BitSet[] bsRows = new BitSet[numOfRows];

		String[][] dataMatrix = new String[numOfRows][];

		EasyFileReader reader = new EasyFileReader(filename);

		int rowCount = -1;
		for (String line : reader)
		{
			++rowCount;
			String[] values = line.split(",");
			dataMatrix[rowCount] = values;
		}

		double[][] dataToConsider = new double[numOfRows][numOfAttr];

		for (int row = 0; row < dataMatrix.length; ++row)
		{
			for (int column = 0; column < numOfAttr; ++column)
			{
				String data = dataMatrix[row][ofset + column];
				dataToConsider[row][column] = parseDouble(data);
			}
		}

		@SuppressWarnings("unchecked")
		List<Double>[] sortedColumns = new ArrayList[numOfAttr];
		for (int columnIx = 0; columnIx < numOfAttr; ++columnIx)
		{
			List<Double> sortedColumn = new ArrayList<Double>(numOfRows);
			for (int rowIx = 0; rowIx < numOfRows; ++rowIx)
			{
				sortedColumn.add(dataToConsider[rowIx][columnIx]);
			}
			Collections.sort(sortedColumn);
			sortedColumns[columnIx] = sortedColumn;
		}

		int step = numOfRows / numOfBitsPerAttr;

		for (int row = 0; row < dataToConsider.length; ++row)
		{
			BitSet rowBS = new BitSet(numOfAttr * numOfBitsPerAttr);
			for (int column = 0; column < numOfAttr; ++column)
			{
				int bitToSet = -1;
				int bitOffset = column * numOfBitsPerAttr;

				for (int i = 0; i < numOfBitsPerAttr; i++)
				{
					double stepValue = sortedColumns[column].get(step * (i + 1) - 1);
					if (dataToConsider[row][column] < stepValue)
					{
						bitToSet = bitOffset + i;
						break;
					} else if (dataToConsider[row][column] == stepValue)
					{
						if (i < numOfBitsPerAttr - 1)
						{
							bitToSet = bitOffset + i + r.nextInt(2);
						} else
						{
							bitToSet = bitOffset + i;
						}
						break;
					}
				}
				if (bitToSet == -1)
				{
					bitToSet = numOfBitsPerAttr - 1;
				}
				rowBS.set(bitToSet);
			}
			bsRows[row] = rowBS;
		}

		for (BitSet row : bsRows)
		{
			System.out.println(BitSetPrinter.asRawString(row, numOfAttr
					* numOfBitsPerAttr));
		}
	}
}
