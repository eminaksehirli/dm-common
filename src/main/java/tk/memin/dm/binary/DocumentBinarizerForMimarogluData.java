package tk.memin.dm.binary;

import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import tk.memin.dm.io.EasyFileReader;

/**
 * This is a utility class to convert <a
 * href="http://www.cs.umb.edu/~smimarog/textmining/datasets/index.html">a
 * specific set of data files</a> to bit sets.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class DocumentBinarizerForMimarogluData
{
	public static void printAsBinary(String filename, int numOfLines,
			int numOfWords)
	{
		int lineCounter = 0;
		int emptyLineCounter = 0;

		EasyFileReader reader = new EasyFileReader(filename);
		Map<String, Integer> wordMap = new HashMap<String, Integer>(numOfLines);

		String[] words;
		int endOffset = 0;

		for (String line : reader)
		{
			lineCounter++;
			BitSet docAsBitSet = new BitSet(numOfWords);

			String[] temp = line.split("\t");
			try
			{
				String document = temp[1];
				words = document.split(" ");
			} catch (ArrayIndexOutOfBoundsException e)
			{
				System.err.println("Line " + lineCounter
						+ " does not contain any words.");
				emptyLineCounter++;
				continue;
			}

			for (String word : words)
			{
				Integer wordIx = wordMap.get(word);
				if (wordIx == null)
				{
					wordIx = wordMap.size() + 1;
					wordMap.put(word, wordIx);
				}
				docAsBitSet.set(wordIx);
			}

			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < numOfWords; ++i)
			{
				if (docAsBitSet.get(i))
				{
					builder.append("1");
				} else
				{
					builder.append("0");
				}
				// builder.append(",");
			}
			// endOffset = 1;
			System.out.println(builder.substring(0, builder.length() - endOffset));
		}
		System.err.println("number of words:" + wordMap.size());
		System.err.println("number of lines:" + lineCounter);
		System.err.println("number of empty lines:" + emptyLineCounter);
	}

	public static void extractCountsFrom(String filename)
	{
		int lineCounter = 0;
		int emptyLineCounter = 0;

		EasyFileReader reader = new EasyFileReader(filename);
		Set<String> wordSet = new HashSet<String>();

		String[] words;

		for (String line : reader)
		{
			lineCounter++;

			String[] temp = line.split("\t");
			try
			{
				String document = temp[1];
				words = document.split(" ");
			} catch (ArrayIndexOutOfBoundsException e)
			{
				System.err.println("Line " + lineCounter
						+ " does not contain any words.");
				emptyLineCounter++;
				continue;
			}

			for (String word : words)
			{
				wordSet.add(word);
			}
		}
		System.err.println("number of words:" + wordSet.size());
		System.err.println("number of lines:" + lineCounter);
		System.err.println("number of empty lines:" + emptyLineCounter);
	}
}
