package tk.memin.dm.binary;

import static java.lang.Double.parseDouble;

import java.util.BitSet;

import tk.memin.dm.io.EasyFileReader;

/**
 * This class binarizes a sub-matrix of a data set using min-max normalization.
 * <code>numOfBitsPerAttr</code> number of bits are used for each
 * <code>numOfAttr</code> number of attributes. Each data is assigned to a bit
 * by determining the linear place of it between min and max values of that
 * column.
 * 
 * @deprecated Experimental class, refactor and clean it if you have to use it
 */
@Deprecated
public class RealDataBinarizerValue
{
	public static void main(String args[])
	{
		String filename = "/home/memin/Belgeler/arastirma/data_mining/data/letter-recognition.data";
		int numOfAttr = 5;
		int ofset = 7;
		int numOfBitsPerAttr = 5;
		int numOfRows = 20000;

		BitSet[] bsRows = new BitSet[numOfRows];

		String[][] dataMatrix = new String[numOfRows][];

		EasyFileReader reader = new EasyFileReader(filename);

		int rowCount = -1;
		for (String line : reader)
		{
			++rowCount;
			String[] values = line.split(",");
			dataMatrix[rowCount] = values;
		}

		double[][] dataToConsider = new double[numOfRows][numOfAttr];
		double[] maxValues = new double[numOfAttr];
		double[] minValues = new double[numOfAttr];
		for (int i = 0; i < minValues.length; ++i)
		{
			minValues[i] = Double.MAX_VALUE;
		}

		for (int row = 0; row < dataMatrix.length; ++row)
		{
			for (int column = 0; column < numOfAttr; ++column)
			{
				String data = dataMatrix[row][ofset + column];
				double cell = parseDouble(data);

				dataToConsider[row][column] = cell;
				if (cell > maxValues[column])
				{
					maxValues[column] = cell;
				}
				if (cell < minValues[column])
				{
					minValues[column] = cell;
				}
			}
		}

		int[] bucketSizes = new int[numOfAttr];

		for (int i = 0; i < bucketSizes.length; ++i)
		{
			// This gives us robustness against not equally sized buckets.
			bucketSizes[i] = (int) (maxValues[i] - minValues[i]) / numOfBitsPerAttr;
		}
		// int step = numOfRows / numOfBitsPerAttr;

		for (int row = 0; row < dataToConsider.length; ++row)
		{
			BitSet rowBS = new BitSet(numOfAttr * numOfBitsPerAttr);
			for (int column = 0; column < numOfAttr; ++column)
			{
				int bitToSet;
				for (bitToSet = 0; bitToSet < numOfBitsPerAttr; ++bitToSet)
				{
					if (dataToConsider[row][column] <= bucketSizes[column]
							* (bitToSet + 1))
					{
						break;
					}
				}

				rowBS.set(column * numOfBitsPerAttr + bitToSet);
			}
			bsRows[row] = rowBS;
		}

		for (BitSet row : bsRows)
		{
			System.out.println(BitSetPrinter.asRawString(row, numOfAttr
					* numOfBitsPerAttr));
		}
	}
}
