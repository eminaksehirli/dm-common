package tk.memin.dm.text;

public class TextProcessor
{
	public static String removeNonChars(String toBeProcessed)
	{
		return toBeProcessed.replaceAll("[^\\w]+", " ").trim();
	}

	public static String removeStopWords(String toBeProcessed)
	{
		StringBuilder sb = new StringBuilder();
		for (String word : toBeProcessed.split(" "))
		{
			if (word.length() <= 0)
			{
				continue;
			}

			if (!StopWords.allWords.contains(word))
			{
				sb.append(word).append(" ");
			}
		}
		return sb.toString().trim();
	}
}
