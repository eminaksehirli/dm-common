package tk.memin.dm.cluster.ensemble;

import java.util.BitSet;

public interface NumberOfClusterEnsembleAlgorithm
{
	public void run(int numOfClusters);

	public abstract BitSet[] finalClustersAsBitSets();

	public abstract int[] finalClusteringAsArray();
}