package tk.memin.dm.cluster.ensemble;

import java.util.BitSet;
import java.util.Collection;

/**
 * Distributes objects between meta-groups, i.e. groups of groups, by assigning
 * the object to a meta-group that has the maximum probability of having the
 * object. The probability is either determined solely by number of occurrences
 * or the ratio of occurrences to the total number of groups in that meta-group.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class MajorityVoter
{
	// FIXME: add the ratio chooser.
	public static BitSet[] voteForMajority(
			Collection<? extends Collection<BitSet>> metaClusters, int numOfObjects)
	{
		int numOfMetaClusters = metaClusters.size();

		int[][] votes = new int[numOfMetaClusters][numOfObjects];
		int metaClusterIx = 0;
		for (Collection<BitSet> metaCluster : metaClusters)
		{
			for (BitSet cluster : metaCluster)
			{
				for (int objIx = cluster.nextSetBit(0); objIx > -1; objIx = cluster.nextSetBit(objIx + 1))
				{
					++votes[metaClusterIx][objIx];
				}
			}
			++metaClusterIx;
		}

		BitSet[] finalClusters = new BitSet[numOfMetaClusters];
		for (int i = 0; i < numOfMetaClusters; ++i)
		{
			finalClusters[i] = new BitSet();
		}

		for (int columnIx = 0; columnIx < numOfObjects; ++columnIx)
		{
			int maxVote = -1;
			int maxVotedCluster = 0;
			for (int rowIx = 0; rowIx < numOfMetaClusters; ++rowIx)
			{
				if (votes[rowIx][columnIx] > maxVote)
				{
					maxVote = votes[rowIx][columnIx];
					maxVotedCluster = rowIx;
				}
			}
			finalClusters[maxVotedCluster].set(columnIx);
		}

		BitSet[] trimmedClusters = removeEmptyClusters(finalClusters);

		return trimmedClusters;
	}

	private static BitSet[] removeEmptyClusters(BitSet[] finalClusters)
	{
		boolean[] clusterIsNotEmpty = new boolean[finalClusters.length];
		int notEmptyClustersCount = 0;
		for (int i = 0; i < finalClusters.length; ++i)
		{
			if (finalClusters[i].cardinality() != 0)
			{
				clusterIsNotEmpty[i] = true;
				++notEmptyClustersCount;
			}
		}

		BitSet[] trimmedClusters = new BitSet[notEmptyClustersCount];
		int count = 0;
		for (int i = 0; i < finalClusters.length; ++i)
		{
			if (clusterIsNotEmpty[i])
			{
				trimmedClusters[count] = finalClusters[i];
				count++;
			}
		}
		return trimmedClusters;
	}
}
