/* This is a utility class for clustering ensemble methods. 
 * 
 * Copyright (C) 2010-2013 Emin Aksehirli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tk.memin.dm.cluster.ensemble;

import static java.lang.Integer.parseInt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import tk.memin.dm.io.EasyFileReader;

/**
 * Extracts partitions or a single partition from given type of data.
 * 
 * @author Emin Aksehirli
 * 
 */
public class ExtractClusterings
{
	public static ClusterInfo fromFile(String filename)
	{
		EasyFileReader reader = new EasyFileReader(filename);
		int objectCount = 0;
		List<BitSet> allClusters = new ArrayList<BitSet>();
		List<BitSet[]> partitions = new LinkedList<BitSet[]>();
		for (String line : reader)
		{
			if (line.trim().length() < 2)
			{
				continue;
			}

			if (line.charAt(0) == '#')
			{
				continue;
			}

			BitSet[] partition = fromString(line);

			objectCount = line.split(",").length;

			partitions.add(partition);

			for (BitSet cluster : partition)
			{
				allClusters.add(cluster);
			}
		}

		ClusterInfo clusterInfo = new ClusterInfo();

		clusterInfo.numOfObjects = objectCount;
		clusterInfo.partitions = partitions;
		clusterInfo.allClusters = allClusters;

		return clusterInfo;
	}

	public static BitSet[] fromClusterFile(String fileName)
	{
		EasyFileReader reader = new EasyFileReader(fileName);
		ArrayList<BitSet> clusters = new ArrayList<BitSet>();

		for (String line : reader)
		{
			BitSet cluster = new BitSet();
			String[] lineArr = line.split(",");
			for (String str : lineArr)
			{
				cluster.set(parseInt(str));
			}
			clusters.add(cluster);
		}
		return clusters.toArray(new BitSet[0]);
	}

	private static BitSet[] fromString(String line)
	{
		String[] items = line.split(",");

		Map<String, BitSet> partition = arrayToBitSets(items);

		partition.remove("N");

		List<BitSet> numericPartition = new ArrayList<BitSet>(partition.values());

		return numericPartition.toArray(new BitSet[0]);
	}

	public static ClusterInfo fromArray(int[][] input)
	{
		return fromArray(Arrays.asList(input));
	}

	// TODO: Boiler plate code. If only there was method pointers in Java.
	/**
	 * <code>0</code> label of an object indicates that the object is not labeled.
	 */
	public static ClusterInfo fromArray(List<int[]> clusterings)
	{
		int objectCount = 0;
		List<BitSet> allClusters = new ArrayList<BitSet>();
		List<BitSet[]> partitions = new LinkedList<BitSet[]>();
		for (int[] clustering : clusterings)
		{
			Integer[] clusteringAsInteger = new Integer[clustering.length];
			for (int i = 0; i < clustering.length; ++i)
			{
				clusteringAsInteger[i] = clustering[i];
			}

			Map<Integer, BitSet> clusters = arrayToBitSets(clusteringAsInteger);
			clusters.remove(0);

			objectCount = clustering.length;

			BitSet[] partitionAsArray = new BitSet[clusters.size()];
			for (Entry<Integer, BitSet> entry : clusters.entrySet())
			{
				Integer key = entry.getKey() - 1;
				try
				{
					partitionAsArray[key] = entry.getValue();
				} catch (ArrayIndexOutOfBoundsException e)
				{
					e.printStackTrace();
				}

			}
			partitions.add(partitionAsArray);

			for (BitSet cluster : partitionAsArray)
			{
				allClusters.add(cluster);
			}
		}

		ClusterInfo clusterInfo = new ClusterInfo();

		clusterInfo.numOfObjects = objectCount;
		clusterInfo.partitions = partitions;
		clusterInfo.allClusters = allClusters;

		return clusterInfo;
	}

	private static <T> Map<T, BitSet> arrayToBitSets(T[] items)
	{
		Map<T, BitSet> partition = new HashMap<T, BitSet>();
		int objectCount = items.length;

		for (int i = 0; i < objectCount; i++)
		{
			BitSet cluster = partition.get(items[i]);
			if (cluster == null)
			{
				cluster = new BitSet(objectCount);
				partition.put(items[i], cluster);
			}
			cluster.set(i);
		}
		return partition;
	}

	public static class ClusterInfo
	{
		public int numOfObjects;
		public List<BitSet[]> partitions;
		public List<BitSet> allClusters;
		private int sizeOfLargestClustering = 0;
		private BitSet[] allClustersAsArrays;

		public int sizeOfLargestClustering()
		{
			if (sizeOfLargestClustering == 0)
			{
				for (BitSet[] partition : partitions)
				{
					if (partition.length > sizeOfLargestClustering)
					{
						sizeOfLargestClustering = partition.length;
					}
				}
			}
			return sizeOfLargestClustering;
		}

		public BitSet[] allClustersAsArray()
		{
			if (allClustersAsArrays == null)
			{
				allClustersAsArrays = allClusters.toArray(new BitSet[0]);
			}
			return allClustersAsArrays;
		}
	}

	public static List<List<BitSet>> indexToMetaCluster(
			Collection<? extends Collection<Integer>> indexedPartitions,
			BitSet[] clusters)
	{
		List<List<BitSet>> metaClusters = new LinkedList<List<BitSet>>();
		for (Collection<Integer> metisPartition : indexedPartitions)
		{
			List<BitSet> metaCluster = new LinkedList<BitSet>();
			for (int clusterIx : metisPartition)
			{
				metaCluster.add(clusters[clusterIx]);
			}
			metaClusters.add(metaCluster);
		}
		return metaClusters;
	}
}
