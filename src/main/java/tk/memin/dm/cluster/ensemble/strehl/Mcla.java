package tk.memin.dm.cluster.ensemble.strehl;

import static tk.memin.dm.distance.DistanceHelper.jaccardSimilarityBetween;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;

import tk.memin.dm.cluster.ensemble.MajorityVoter;
import tk.memin.dm.cluster.ensemble.NumberOfClusterEnsembleAlgorithm;
import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;

/**
 * Meta-Cluster Partitioning Algorithm (MCLA) from the paper <a
 * href="http://dx.doi.org/10.1162/153244303321897735">
 * "Cluster ensembles --- a knowledge reuse framework for combining multiple partitions"
 * by Alexander Strehl and Joydeep Ghosh</a>. The algorithms combines multiple
 * clusterings by creating a graph representation of clusterings where each
 * vertex is a cluster and weighted edges between them are the Jaccard
 * similarities of them. Then runs the Metis graph partitioning algorithm over
 * graph. See the original paper for details.
 * 
 * 
 * @author M. Emin Aksehirli
 */
public class Mcla implements NumberOfClusterEnsembleAlgorithm
{
	private BitSet[] clusters;
	private int numOfClusters;
	private int numOfObjects;
	public BitSet[] finalClusters;

	public Mcla(ClusterInfo clusterInfo)
	{
		clusters = clusterInfo.allClusters.toArray(new BitSet[0]);
		numOfClusters = clusters.length;
		numOfObjects = clusterInfo.numOfObjects;
	}

	@Override
	public void run(int numOfFinalClusters)
	{
		double[][] graphMatrix = new double[numOfClusters][numOfClusters];

		for (int i = 0; i < numOfClusters; ++i)
		{
			for (int j = i + 1; j < numOfClusters; ++j)
			{
				graphMatrix[i][j] = graphMatrix[j][i] = jaccardSimilarityBetween(
						clusters[i], clusters[j]);
			}
		}

		MetisOperations metis = new MetisOperations("mcla_out");
		metis.prepareFileForMetis(graphMatrix);
		metis.runMetis(numOfFinalClusters);
		List<List<Integer>> metisPartitions = metis.readMetisPartitions(numOfFinalClusters);

		List<List<BitSet>> metaClusters = new LinkedList<List<BitSet>>();

		for (List<Integer> metisPartition : metisPartitions)
		{
			List<BitSet> metaCluster = new LinkedList<BitSet>();
			for (int clusterIx : metisPartition)
			{
				metaCluster.add(clusters[clusterIx]);
			}
			metaClusters.add(metaCluster);
		}

		finalClusters = MajorityVoter.voteForMajority(metaClusters, numOfObjects);
	}

	@Override
	public BitSet[] finalClustersAsBitSets()
	{
		return finalClusters;
	}

	@Override
	public int[] finalClusteringAsArray()
	{
		return finalClusteringAsArray(0);
	}

	/**
	 * 
	 * @param offset
	 *          It is used to change the starting index.
	 * @return
	 */
	public int[] finalClusteringAsArray(int offset)
	{
		int[] finalArray = new int[numOfObjects];
		for (int i = 0; i < finalClusters.length; ++i)
		{
			BitSet cluster = finalClusters[i];
			int clusterNo = i + offset;
			for (int j = cluster.nextSetBit(0); j >= 0; j = cluster.nextSetBit(j + 1))
			{
				finalArray[j] = clusterNo;
			}
		}
		return finalArray;
	}
}
