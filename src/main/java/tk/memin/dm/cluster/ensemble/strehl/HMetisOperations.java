package tk.memin.dm.cluster.ensemble.strehl;

import static java.lang.Integer.parseInt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;

import tk.memin.dm.io.EasyFileReader;

/**
 * Wrapper class for HMetis. Beware that this implementation is not space
 * effective. If you will get <code>OutOfMemory</code> errors you can make some
 * changes to get rid of it.
 * 
 * It requires hmetis executables in a directory that can be reachable from
 * code. hmetis can be downloaded from <a href="http:
 * //glaros.dtc.umn.edu/gkhome/">Karypis Lab</a>.
 * 
 * Update the constant <code>HMetis_Directory</code> according to directory of
 * HMetis.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class HMetisOperations
{
	private static final String HMetis_Directory = "hmetis";
	private String metisOutFile;
	public boolean printOut = false;

	public HMetisOperations(String metisOutFile)
	{
		this.metisOutFile = metisOutFile;
	}

	public void prepareFileForMetis(BitSet[] edges)
	{
		List<String> hmetisEdgeStrings = new ArrayList<String>(edges.length);

		int numOfObjects = 0;
		for (BitSet edge : edges)
		{
			int length = edge.length();
			if (length > numOfObjects)
			{
				numOfObjects = length;
			}

			StringBuilder edgeString = new StringBuilder();
			for (int i = edge.nextSetBit(0); i >= 0; i = edge.nextSetBit(i + 1))
			{
				edgeString.append(i + 1).append(" ");
			}
			hmetisEdgeStrings.add(edgeString.toString());
		}

		int numOfVertices = numOfObjects;

		try
		{
			FileWriter outFile = new FileWriter(metisOutFile);
			outFile.write(edges.length + " " + numOfVertices + "\n");
			for (String edge : hmetisEdgeStrings)
			{
				outFile.write(edge + "\n");
			}
			outFile.close();
		} catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}

	public void runMetis(int numOfMetisPartitions, int balanceFactor)
	{
		try
		{
			Process p = Runtime.getRuntime().exec(
					HMetis_Directory + "/shmetis" + " " + metisOutFile + " "
							+ numOfMetisPartitions + " " + balanceFactor);

			p.waitFor();
			printTheOutputOf(p);
		} catch (Exception e)
		{
			throw new IllegalStateException(e);
		}
		File f = new File(metisOutFile);
		if (!f.delete())
		{
			System.err.println(metisOutFile + " can not be deleted!");
		}
	}

	private void printTheOutputOf(Process p) throws IOException
	{
		readAndWrite(p.getInputStream(), System.out);
		readAndWrite(p.getErrorStream(), System.err);
	}

	private void readAndWrite(InputStream inputStream, PrintStream out)
			throws IOException
	{
		InputStreamReader isr = new InputStreamReader(inputStream);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null)
		{
			if (printOut)
			{
				out.println(line);
			}
		}
	}

	public List<List<Integer>> readMetisPartitions(int numOfMetisPartitions)
			throws NumberFormatException
	{
		String fileName = metisOutFile + ".part." + numOfMetisPartitions;
		EasyFileReader metisReader = new EasyFileReader(fileName);

		List<List<Integer>> metisPartitions = new ArrayList<List<Integer>>(numOfMetisPartitions);
		for (int i = 0; i < numOfMetisPartitions; ++i)
		{
			metisPartitions.add(new LinkedList<Integer>());
		}

		int lineCounter = -1;
		for (String line : metisReader)
		{
			++lineCounter;
			metisPartitions.get(parseInt(line.trim())).add(lineCounter);
		}

		File f = new File(fileName);
		if (!f.delete())
		{
			System.err.println(fileName + " can not be deleted!");
		}
		return metisPartitions;
	}

}
