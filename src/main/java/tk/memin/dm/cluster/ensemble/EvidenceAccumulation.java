package tk.memin.dm.cluster.ensemble;

import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tk.memin.dm.cluster.agnes.Agnes;
import tk.memin.dm.cluster.agnes.AgnesMergerFactory;
import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;

/**
 * Evidince Accumulation from the paper <a
 * href="http://dx.doi.org/10.1109/ICPR.2002.1047450"> " Data clustering using
 * evidence accumulation by Fred and Jain</a>. The algorithms combines multiple
 * clusterings by creating co-occurance matrix of objects (evidence
 * accumulation) and clusters this matrix using single linkage (SL) hierarchical
 * clustering algorithm.
 * 
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class EvidenceAccumulation implements NumberOfClusterEnsembleAlgorithm
{
	private int numOfObjects;
	private BitSet[] clusters;
	private BitSet[] clustersOfObjects;
	private Agnes agnes;

	public EvidenceAccumulation(ClusterInfo clusterInfo)
	{
		numOfObjects = clusterInfo.numOfObjects;
		clusters = clusterInfo.allClusters.toArray(new BitSet[0]);

		clustersOfObjects = new BitSet[numOfObjects];
		for (int i = 0; i < numOfObjects; ++i)
		{
			clustersOfObjects[i] = new BitSet();
		}

		for (int i = 0; i < clusters.length; ++i)
		{
			BitSet cluster = clusters[i];
			for (int objIx = cluster.nextSetBit(0); objIx >= 0; objIx = cluster.nextSetBit(objIx + 1))
			{
				clustersOfObjects[objIx].set(i);
			}
		}
	}

	@Override
	public void run(int numOfFinalClusters)
	{
		double[][] adjacencyMatrix = new double[numOfObjects][numOfObjects];

		BitSet acc = new BitSet();
		for (int i = 0; i < clustersOfObjects.length; ++i)
		{
			for (int j = i + 1; j < clustersOfObjects.length; ++j)
			{
				acc.clear();
				acc.or(clustersOfObjects[i]);
				acc.and(clustersOfObjects[j]);
				adjacencyMatrix[i][j] = adjacencyMatrix[j][i] = acc.cardinality();
			}
		}

		agnes = new Agnes(adjacencyMatrix, AgnesMergerFactory.averageLink());
		adjacencyMatrix = null;
		agnes.run(numOfFinalClusters);
	}

	@Override
	public BitSet[] finalClustersAsBitSets()
	{
		return agnes.clustersAsBitSets().toArray(new BitSet[0]);
	}

	@Override
	public int[] finalClusteringAsArray()
	{
		return finalClusteringAsArray(0);
	}

	/**
	 * 
	 * @param ofset
	 *          It is used to change the starting index.
	 * @return
	 */

	public int[] finalClusteringAsArray(int ofset)
	{
		int[] finalArray = new int[numOfObjects];

		int clusterNo = 0;

		final List<HashSet<Integer>> agnesClusters = agnes.clusters();
		for (Set<Integer> finalCluster : agnesClusters)
		{
			for (int objectIx : finalCluster)
			{
				finalArray[objectIx] = clusterNo + ofset;
			}
			clusterNo++;
		}
		return finalArray;
	}

}
