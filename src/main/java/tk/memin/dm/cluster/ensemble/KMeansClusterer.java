package tk.memin.dm.cluster.ensemble;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tk.memin.dm.cluster.ensemble.ExtractClusterings;
import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;
import tk.memin.dm.cluster.kmeans.KMeans;

/**
 * Runs k-means on vector matrix several times. This class is initially used to
 * generate input clusterings for cluster ensembles.
 * 
 * @author M. Emin Aksehirli
 * 
 */

// FIXME: we have to check the input parameters in case they are not set.
public class KMeansClusterer
{
	private int numberOfInputClusterings;
	private double[][] matrix;
	private int[] numOfClusters;
	private int maxNumOfClusters;
	private double minSpace;
	private double maxSpace;
	private double minSamples;
	private double maxSamples;
	private boolean featureCalculationNeeded = true;
	private boolean spaceCalculationNeeded = true;
	private boolean[] ignoreThisFeature;
	private boolean[] ignoreThisSample;

	/**
	 * Runs k-means on vector matrix several times. This class is initially used
	 * to generate input clusterings for cluster ensembles.
	 * 
	 * @param matrix
	 *          Each row is a data object and each column is a feature.
	 * @param numberOfInputClusterings
	 *          Number of clusterings to generate. Note that it's not number of
	 *          clusters, it's number of clusterINGs.
	 */
	public KMeansClusterer(double[][] matrix, int numberOfInputClusterings)
	{
		this.numberOfInputClusterings = numberOfInputClusterings;
		this.matrix = matrix;
		this.numOfClusters = new int[numberOfInputClusterings];
		this.maxNumOfClusters = (int) Math.sqrt(matrix.length);
	}

	public ClusterInfo generateInputClusterings()
	{
		if (numOfClusters[0] < 1)
		{
			throw new IllegalStateException("Clustering parameters are not set! "
					+ "kMeansClusterer.set(InputParameters) should have been called!");
		}

		KMeans kMeans = KMeans.kMeansDefault();
		List<int[]> inputClusterings = new ArrayList<int[]>(numberOfInputClusterings);

		for (int i = 0; i < numberOfInputClusterings; ++i)
		{
			double[][] toBeProcessedMatrix = matrix;
			if (featureCalculationNeeded || spaceCalculationNeeded)
			{
				// System.err.println("calculating a sub matrix");
				toBeProcessedMatrix = calculateSubMatrix();
			}

			int[] input = kMeans.run(toBeProcessedMatrix, numOfClusters[i]);
			if (featureCalculationNeeded || spaceCalculationNeeded)
			{
				int[] correctedClustering = addIgnoredSamples(input);
				inputClusterings.add(correctedClustering);
			} else
			{
				inputClusterings.add(input);
			}
		}

		prepareForExtractClustering(inputClusterings);

		ClusterInfo inputClusterInfo = ExtractClusterings.fromArray(inputClusterings);
		return inputClusterInfo;
	}

	private static void prepareForExtractClustering(List<int[]> inputClusterings)
	{
		for (int[] inputClustering : inputClusterings)
		{
			for (int i = 0; i < inputClustering.length; ++i)
			{
				inputClustering[i] = inputClustering[i] + 1;
			}
		}
	}

	private double[][] calculateSubMatrix()
	{
		Random random = new Random();

		int numOfSelectedFeatures = (int) (matrix[0].length * (minSpace + (random.nextDouble() * (maxSpace - minSpace))));
		int numOfSelectedSamples = (int) (matrix.length * (minSamples + (random.nextDouble() * (maxSamples - minSamples))));

		double[][] toBeProcessedMatrix = new double[numOfSelectedSamples][numOfSelectedFeatures];

		ignoreThisFeature = new boolean[matrix[0].length];
		ignoreThisSample = new boolean[matrix.length];

		int randomIx;
		for (int i = 0; i < matrix[0].length - numOfSelectedFeatures; ++i)
		{
			do
			{
				randomIx = random.nextInt(matrix[0].length);
			} while (ignoreThisFeature[randomIx]);
			ignoreThisFeature[randomIx] = true;
		}

		for (int i = 0; i < matrix.length - numOfSelectedSamples; ++i)
		{
			do
			{
				randomIx = random.nextInt(matrix.length);
			} while (ignoreThisSample[randomIx]);
			ignoreThisSample[randomIx] = true;
		}

		for (int sampleIx = 0, newSampleIx = 0; sampleIx < matrix.length; ++sampleIx)
		{
			if (!ignoreThisSample[sampleIx])
			{
				for (int featureIx = 0, newFeatureIx = 0; featureIx < matrix[0].length; ++featureIx)
				{
					if (!ignoreThisFeature[featureIx])
					{
						toBeProcessedMatrix[newSampleIx][newFeatureIx] = matrix[sampleIx][featureIx];
						++newFeatureIx;
					}
				}
				++newSampleIx;
			}
		}
		return toBeProcessedMatrix;
	}

	private int[] addIgnoredSamples(int[] anInputClustering)
	{
		int[] correctedClustering = new int[ignoreThisSample.length];

		for (int oldIx = 0, newIx = 0; newIx < matrix.length; ++newIx)
		{
			if (!ignoreThisSample[newIx])
			{
				correctedClustering[newIx] = anInputClustering[oldIx];
				oldIx++;
			}
		}
		return correctedClustering;
	}

	public void fixTheNumberOfClusters()
	{
		for (int i = 0; i < numOfClusters.length; ++i)
		{
			numOfClusters[i] = maxNumOfClusters;
		}
	}

	public void randomizeTheNumberOfClusters()
	{
		Random random = new Random();
		for (int i = 0; i < numOfClusters.length; ++i)
		{
			numOfClusters[i] = 2 + random.nextInt(maxNumOfClusters - 1);
		}
	}

	public void setSpaceBetween(double min, double max)
	{
		this.minSpace = min;
		this.maxSpace = max;

		if (min == 1 && max == 1)
		{
			spaceCalculationNeeded = false;
		}
	}

	public void setSamplingBetween(double min, double max)
	{
		this.minSamples = min;
		this.maxSamples = max;

		if (min == 1 && max == 1)
		{
			featureCalculationNeeded = false;
		}
	}

	public void set(InputParameters inputParams)
	{
		setSpaceBetween(inputParams.minSpace, inputParams.maxSpace);
		setSamplingBetween(inputParams.minSamples, inputParams.maxSamples);
		if (inputParams.fixedK)
		{
			fixTheNumberOfClusters();
		} else
		{
			randomizeTheNumberOfClusters();
		}
	}

	public static class InputParameters
	{
		public String fileName;
		public double minSamples;
		public double maxSamples;
		public double minSpace;
		public double maxSpace;
		public boolean fixedK;

		public InputParameters(InputParameters inputParameters)
		{
			this.fileName = inputParameters.fileName;
			this.minSamples = inputParameters.minSamples;
			this.maxSamples = inputParameters.maxSamples;
			this.minSpace = inputParameters.minSpace;
			this.maxSpace = inputParameters.maxSpace;
			this.fixedK = inputParameters.fixedK;
		}

		public InputParameters()
		{
		}

		public void setSpaceBetween(double min, double max)
		{
			this.minSpace = min;
			this.maxSpace = max;
		}

		public void setSamplingBetween(double min, double max)
		{
			this.minSamples = min;
			this.maxSamples = max;
		}

		@Override
		public String toString()
		{
			StringBuilder sb = new StringBuilder(fileName.substring(0, 15));
			sb.append("\t");
			if (fixedK)
			{
				sb.append("FK");
			} else
			{
				sb.append("RK");
			}
			sb.append(String.format("%.2f%.2fF", minSpace, maxSpace));
			sb.append(String.format("%.2f%.2fS", minSamples, maxSamples));

			return sb.toString();
		}
	}
}
