package tk.memin.dm.cluster.ensemble.strehl;

import java.util.BitSet;
import java.util.List;

import tk.memin.dm.cluster.ensemble.NumberOfClusterEnsembleAlgorithm;
import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;

/**
 * Cluster Similarity based Partitioning Algorithm (CSPA) from the paper <a
 * href="http://dx.doi.org/10.1162/153244303321897735">
 * "Cluster ensembles --- a knowledge reuse framework for combining multiple partitions"
 * by Alexander Strehl and Joydeep Ghosh</a>. The algorithms combines multiple
 * clusterings by creating a graph representation of clusterings where each
 * vertex is a data point and weighted edges between them are the co-occurances
 * of them. Then runs the Metis graph partitioning algorithm over graph. See the
 * original paper for details.
 * 
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class Cspa implements NumberOfClusterEnsembleAlgorithm
{
	private int numOfObjects;
	private BitSet[] clusters;
	public List<List<Integer>> finalClusters;
	private BitSet[] clustersOfObjects;
	private BitSet[] finalClustersAsBitSets;

	public Cspa(ClusterInfo clusterInfo)
	{
		numOfObjects = clusterInfo.numOfObjects;
		clusters = clusterInfo.allClusters.toArray(new BitSet[0]);

		clustersOfObjects = new BitSet[numOfObjects];
		for (int i = 0; i < numOfObjects; ++i)
		{
			clustersOfObjects[i] = new BitSet();
		}

		for (int i = 0; i < clusters.length; ++i)
		{
			BitSet cluster = clusters[i];
			for (int objIx = cluster.nextSetBit(0); objIx >= 0; objIx = cluster.nextSetBit(objIx + 1))
			{
				clustersOfObjects[objIx].set(i);
			}
		}
	}

	@Override
	public void run(int numOfFinalClusters)
	{
		int[][] adjacencyMatrix = new int[numOfObjects][numOfObjects];

		BitSet acc = new BitSet();
		for (int i = 0; i < clustersOfObjects.length; ++i)
		{
			for (int j = i + 1; j < clustersOfObjects.length; ++j)
			{
				acc.clear();
				acc.or(clustersOfObjects[i]);
				acc.and(clustersOfObjects[j]);
				adjacencyMatrix[i][j] = adjacencyMatrix[j][i] = acc.cardinality();
			}
		}

		MetisOperations metis = new MetisOperations("cspa_out");
		metis.prepareFileForMetis(adjacencyMatrix);
		adjacencyMatrix = null;
		metis.runMetis(numOfFinalClusters);
		finalClusters = metis.readMetisPartitions(numOfFinalClusters);
	}

	@Override
	public BitSet[] finalClustersAsBitSets()
	{
		if (finalClustersAsBitSets == null)
		{
			finalClustersAsBitSets = new BitSet[finalClusters.size()];
			int i = 0;
			for (List<Integer> cluster : finalClusters)
			{
				BitSet clusterAsBitSet = new BitSet();
				for (int item : cluster)
				{
					clusterAsBitSet.set(item);
				}
				finalClustersAsBitSets[i++] = clusterAsBitSet;
			}
		}
		return finalClustersAsBitSets;
	}

	@Override
	public int[] finalClusteringAsArray()
	{
		return finalClusteringAsArray(0);
	}

	/**
	 * 
	 * @param ofset
	 *          It is used to change the starting index.
	 * @return
	 */
	public int[] finalClusteringAsArray(int ofset)
	{
		int[] finalArray = new int[numOfObjects];

		int clusterNo = 0;

		for (List<Integer> finalCluster : finalClusters)
		{
			for (int objectIx : finalCluster)
			{
				finalArray[objectIx] = clusterNo + ofset;
			}
			clusterNo++;
		}
		return finalArray;
	}
}
