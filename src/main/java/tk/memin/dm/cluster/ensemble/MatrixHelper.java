package tk.memin.dm.cluster.ensemble;

import java.util.BitSet;
import java.util.Collection;
import java.util.List;

import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;
import tk.memin.dm.cluster.evaluator.ECSEvaluator;
import tk.memin.dm.distance.ProximityCalculatorGeneric;

/**
 * Creates either ecsSimilarity or co-occurrence similarity matrices.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class MatrixHelper
{
	private MatrixHelper()
	{
	}

	public static final int[][] associationFrom(String filename)
	{
		ClusterInfo clusterInfo = ExtractClusterings.fromFile(filename);

		return associationFrom(clusterInfo);
	}

	public static int[][] associationFrom(ClusterInfo clusterInfo)
	{
		int objectCount = clusterInfo.numOfObjects;
		List<BitSet> allClusters = clusterInfo.allClusters;

		int[][] assocMatrix = new int[objectCount][objectCount];
		for (BitSet cluster : allClusters)
		{
			for (int i = cluster.nextSetBit(0); i >= 0; i = cluster.nextSetBit(i + 1))
			{
				for (int j = cluster.nextSetBit(i + 1); j >= 0; j = cluster.nextSetBit(j + 1))
				{
					assocMatrix[i][j]++;
					assocMatrix[j][i]++;
				}
				assocMatrix[i][i]++;
			}
		}
		return assocMatrix;
	}

	public static final double[][] ecsFrom(String filename)
	{
		ClusterInfo clusterInfo = ExtractClusterings.fromFile(filename);

		return ecsFrom(clusterInfo);
	}

	public static double[][] ecsFrom(ClusterInfo clusterInfo)
	{
		BitSet[] clusters = clusterInfo.allClustersAsArray();
		int numOfClusters = clusters.length;

		ECSEvaluator ecs = new ECSEvaluator(clusterInfo);

		double[][] similarityMatrix = new double[numOfClusters][numOfClusters];
		for (int i = 0; i < numOfClusters; i++)
		{
			for (int j = i; j < numOfClusters; j++)
			{
				similarityMatrix[i][j] = similarityMatrix[j][i] = ecs.evaluate(
						clusters[i], clusters[j]);
			}
		}
		return similarityMatrix;
	}

	public static <T> double[][] similarityMatrixFrom(Collection<T> objects,
			ProximityCalculatorGeneric<T> proximityCalculator)
	{
		double[][] simMatrix = new double[objects.size()][objects.size()];

		int ix_1 = 0;
		for (T obj_1 : objects)
		{
			int ix_2 = 0;
			for (T obj_2 : objects)
			{
				simMatrix[ix_1][ix_2] = proximityCalculator.similarityBetween(obj_1,
						obj_2);
				ix_2++;
			}
			ix_1++;
		}

		return simMatrix;
	}

}
