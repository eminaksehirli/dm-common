package tk.memin.dm.cluster.ensemble;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;

import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;
import tk.memin.dm.cluster.evaluator.AdjustedRandIndex;

public class InputClusteringInfo
{
	public HashSet<Integer> partitionSizes;
	public List<Double> aris;
	public double totalInputARI;
	public double maxInputARI;
	public double minInputARI;
	public double avgInputARI;

	public InputClusteringInfo(ClusterInfo inputClusters, BitSet[] targetPartition)
	{
		partitionSizes = new HashSet<Integer>();
		aris = new ArrayList<Double>();

		totalInputARI = 0;
		maxInputARI = 0;
		minInputARI = Integer.MAX_VALUE;

		for (BitSet[] partition : inputClusters.partitions)
		{
			double ari = AdjustedRandIndex.between(partition, targetPartition);

			aris.add(ari);
			totalInputARI += ari;
			if (ari > maxInputARI)
				maxInputARI = ari;
			if (ari < minInputARI)
				minInputARI = ari;

			partitionSizes.add(partition.length);
		}

		avgInputARI = totalInputARI / inputClusters.partitions.size();
	}

}
