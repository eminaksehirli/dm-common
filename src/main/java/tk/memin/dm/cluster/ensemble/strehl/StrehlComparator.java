package tk.memin.dm.cluster.ensemble.strehl;

import static java.lang.System.currentTimeMillis;

import java.util.BitSet;

import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;
import tk.memin.dm.cluster.evaluator.AdjustedRandIndex;

/**
 * This class is used to run all of the Strehl's methods over input clusterings.
 * This is a Helper class that is supposed to be used in tests.
 * 
 * @author M. Emin Aksehirli
 */

public class StrehlComparator
{
	private ClusterInfo inputClusters;
	private Mcla mcla;
	private Cspa cspa;
	private Hgpa hgpa;
	public double mclaAri;
	public double cspaAri;
	public double hgpaAri;
	public long mclaRuntime;
	public long cspaRuntime;
	public long hgpaRunTime;
	private int numberOfClusters;
	private boolean mclaIsDone;
	private boolean cspaIsDone;
	private boolean hgpaIsDone;

	public StrehlComparator(ClusterInfo inputClusters)
	{
		this.inputClusters = inputClusters;
	}

	public void runAll(int numberOfClusters)
	{
		this.numberOfClusters = numberOfClusters;
		mcla = new Mcla(inputClusters);
		hgpa = new Hgpa(inputClusters);
		cspa = new Cspa(inputClusters);

		try
		{
			long start = currentTimeMillis();
			mcla.run(numberOfClusters);
			mclaRuntime = currentTimeMillis() - start;
			mclaIsDone = true;
		} catch (OutOfMemoryError e)
		{
			System.err.println("MCLA could not make it!");
			e.printStackTrace();
		}

		try
		{
			long start = currentTimeMillis();
			cspa.run(numberOfClusters);
			cspaRuntime = currentTimeMillis() - start;
			cspaIsDone = true;
		} catch (OutOfMemoryError e)
		{
			System.err.println("CSPA could not make it!");
			e.printStackTrace();
		}

		try
		{
			long start = currentTimeMillis();
			hgpa.run(numberOfClusters);
			hgpaRunTime = currentTimeMillis() - start;
			hgpaIsDone = true;
		} catch (OutOfMemoryError e)
		{
			System.err.println("HGPA could not make it!");
			e.printStackTrace();
		}
	}

	public void evaluateArisAndPrint(BitSet[] targetPartition)
	{
		evaluateAris(targetPartition);

		System.out.println("for " + numberOfClusters + " clusters:");
		System.out.printf(" mcla: %1.3f, cspa: %1.3f, hgpa: %1.3f", mclaAri,
				cspaAri, hgpaAri);
	}

	public void evaluateAris(BitSet[] targetPartition)
	{
		if (mclaIsDone)
			mclaAri = AdjustedRandIndex.between(mcla.finalClusters, targetPartition);

		if (cspaIsDone)
			cspaAri = AdjustedRandIndex.between(cspa.finalClustersAsBitSets(), targetPartition);

		if (hgpaIsDone)
			hgpaAri = AdjustedRandIndex.between(hgpa.finalClustersAsBitSets(), targetPartition);
	}

	public BitSet[] mclaFinalClusters()
	{
		return mcla.finalClusters;
	}

	public BitSet[] cspaFinalClusters()
	{
		return cspa.finalClustersAsBitSets();
	}

	public BitSet[] hgpaFinalClusters()
	{
		return hgpa.finalClustersAsBitSets();
	}

}
