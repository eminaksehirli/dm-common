package tk.memin.dm.cluster.ensemble.strehl;

import static java.lang.Integer.parseInt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import tk.memin.dm.io.EasyFileReader;

/**
 * Wrapper class for Metis. Beware that this implementation is not very space
 * efficient. If you will get <code>OutOfMemory</code> errors you can make some
 * changes to get rid of it.
 * 
 * It requires metis executables in a directory that can be reachable from code.
 * metis can be downloaded from <a href="http:
 * //glaros.dtc.umn.edu/gkhome/">Karypis Lab</a>.
 * 
 * Update the constant <code>Metis_Executable</code> according to directory of
 * Metis.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class MetisOperations
{
	private static final String Metis_Executable = "metis-4.0/pmetis";
	private String metisOutFile;
	public boolean printOut = false;

	public MetisOperations(String metisOutFile)
	{
		this.metisOutFile = metisOutFile;
	}

	public void prepareFileForMetis(double[][] graph)
	{
		int numOfVertices = graph.length;

		int[][] intGraph = new int[numOfVertices][numOfVertices];

		for (int i = 0; i < numOfVertices; ++i)
		{
			for (int j = i + 1; j < numOfVertices; ++j)
			{
				// because metis works only with integer weights
				intGraph[i][j] = intGraph[j][i] = (int) (graph[i][j] * 1000);
			}
		}

		prepareFileForMetis(intGraph);
	}

	void prepareFileForMetis(int[][] graph)
	{
		int numOfVertices = graph.length;
		List<String> metisEdgeStrings = new ArrayList<String>(numOfVertices);

		int numOfEdges = 0;

		for (int i = 0; i < numOfVertices; ++i)
		{
			StringBuilder edgeString = new StringBuilder();
			for (int j = 0; j < numOfVertices; ++j)
			{
				if (graph[i][j] > 0 && i != j)
				{
					edgeString.append(j + 1).append(" ").append(graph[i][j]).append(" ");
					++numOfEdges;
				}
			}
			metisEdgeStrings.add(edgeString.toString());
		}

		numOfEdges /= 2;

		try
		{
			FileWriter outFile = new FileWriter(metisOutFile);
			outFile.write(numOfVertices + " " + numOfEdges + " " + "1" + "\n");
			for (String edge : metisEdgeStrings)
			{
				outFile.write(edge + "\n");
			}
			outFile.close();
		} catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}

	public void runMetis(int numOfMetisPartitions) throws IllegalStateException
	{
		try
		{
			Process p = Runtime.getRuntime().exec(
					Metis_Executable + " " + metisOutFile + " " + numOfMetisPartitions);

			p.waitFor();
			printTheOutputOf(p);
		} catch (Exception e)
		{
			throw new IllegalStateException(e);
		}

		File file = new File(metisOutFile);
		if (!file.delete())
		{
			System.err.println(metisOutFile + " can not be deleted!");
		}
	}

	private void printTheOutputOf(Process p) throws IOException
	{
		readAndWrite(p.getInputStream(), System.out);
		readAndWrite(p.getErrorStream(), System.err);
	}

	private void readAndWrite(InputStream inputStream, PrintStream out)
			throws IOException
	{
		InputStreamReader isr = new InputStreamReader(inputStream);
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null)
		{
			if (printOut)
			{
				out.println(line);
			}
		}
	}

	public List<List<Integer>> readMetisPartitions(int numOfMetisPartitions)
			throws NumberFormatException
	{
		String fileName = metisOutFile + ".part." + numOfMetisPartitions;
		EasyFileReader metisReader = new EasyFileReader(fileName);

		List<List<Integer>> metisPartitions = new ArrayList<List<Integer>>(numOfMetisPartitions);
		for (int i = 0; i < numOfMetisPartitions; ++i)
		{
			metisPartitions.add(new LinkedList<Integer>());
		}

		int lineCounter = -1;
		for (String line : metisReader)
		{
			++lineCounter;
			metisPartitions.get(parseInt(line.trim())).add(lineCounter);
		}

		File file = new File(fileName);
		if (!file.delete())
		{
			System.err.println(fileName + " can not be deleted!");
		}
		return metisPartitions;
	}
}
