package tk.memin.dm.cluster.ensemble.strehl;

import java.util.BitSet;
import java.util.List;

import tk.memin.dm.cluster.ensemble.NumberOfClusterEnsembleAlgorithm;
import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;

/**
 * Hyper Graph Partitioning Algorithm (HGPA) from the paper <a
 * href="http://dx.doi.org/10.1162/153244303321897735">
 * "Cluster ensembles --- a knowledge reuse framework for combining multiple partitions"
 * by Alexander Strehl and Joydeep Ghosh</a>. The algorithms combines multiple
 * clusterings by creating a Hyper Graph on object level where each Hyper Edge
 * represents a cluster. Then runs HMetis over the graph. See the original paper
 * for details.
 * 
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class Hgpa implements NumberOfClusterEnsembleAlgorithm
{
	private static final int default_Balance_Factor = 5;
	private List<BitSet> clusters;
	List<List<Integer>> finalClusters;
	private BitSet[] finalClustersAsBitSets;
	private int numOfObjects;

	public Hgpa(ClusterInfo clusterInfo)
	{
		clusters = clusterInfo.allClusters;
		numOfObjects = clusterInfo.numOfObjects;
	}

	@Override
	public void run(int numOfMetisPartitions)
	{
		HMetisOperations metis = new HMetisOperations("hgpa_out");
		metis.prepareFileForMetis(clusters.toArray(new BitSet[0]));

		metis.runMetis(numOfMetisPartitions, default_Balance_Factor);

		finalClusters = metis.readMetisPartitions(numOfMetisPartitions);
	}

	@Override
	public BitSet[] finalClustersAsBitSets()
	{
		if (finalClustersAsBitSets == null)
		{
			finalClustersAsBitSets = new BitSet[finalClusters.size()];
			int i = 0;
			for (List<Integer> cluster : finalClusters)
			{
				BitSet clusterAsBitSet = new BitSet();
				for (int item : cluster)
				{
					clusterAsBitSet.set(item);
				}
				finalClustersAsBitSets[i++] = clusterAsBitSet;
			}
		}
		return finalClustersAsBitSets;
	}
	
	@Override
	public int[] finalClusteringAsArray()
	{
		return finalClusteringAsArray(0);
	}

	/**
	 * 
	 * @param ofset
	 *          It is used to change the starting index.
	 * @return
	 */

	public int[] finalClusteringAsArray(int ofset)
	{
		int[] finalArray = new int[numOfObjects];

		int clusterNo = 0;

		for (List<Integer> finalCluster : finalClusters)
		{
			for (int objectIx : finalCluster)
			{
				finalArray[objectIx] = clusterNo + ofset;
			}
			clusterNo++;
		}
		return finalArray;
	}
}
