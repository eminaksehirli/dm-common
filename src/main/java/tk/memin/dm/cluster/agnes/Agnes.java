package tk.memin.dm.cluster.agnes;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;

/**
 * AGlomerative NESting algorithm. Works on a similarity matrix, so any metric
 * can be used as a similarity.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class Agnes
{
	public static final int MaxIterations = Integer.MAX_VALUE;
	public static boolean logout = true;

	private double[][] currentMatrix;
	double[][] initialSimilarityMatrix;
	private int numOfClusters;
	private int maxIndex_1;
	private int maxIndex_2;
	private Cluster[] clusters;
	private double[] afterMergeSimilarities;
	private boolean weFoundAMaximum;

	private ClusterMerger merger;

	/**
	 * AGlomerative NESting algorithm. Generates the matrix at initialisation. May
	 * not be memory optimised.
	 * 
	 * @param similarityMatrix
	 *          Similarity matrix that the algorithm will work on. This matrix
	 *          must remain untouched.
	 * @param method
	 *          Method to calculate the new distances. Enum type.
	 */
	public Agnes(double[][] similarityMatrix, ClusterMerger merger)
	{
		numOfClusters = similarityMatrix.length;
		generateInitialMatrix(similarityMatrix);
		this.merger = merger;
		this.merger.setAlgorithm(this);

		clusters = new Cluster[numOfClusters];
		for (int i = 0; i < numOfClusters; ++i)
		{
			clusters[i] = new Cluster();
			clusters[i].add(i);
		}

		initialSimilarityMatrix = similarityMatrix;
	}

	public void run(int numOfDesiredClusters)
	{
		do
		{
			findMaximum();

			if (!weFoundAMaximum)
			{
				break;
			}

			// printMatrix();
			//
			// println("min_1: " + maxIndex_1);
			// println("min_2: " + maxIndex_2);
			// println(clusters[maxIndex_1] + " + " + clusters[maxIndex_2]);
			mergeClustersAndSimilarities();

			removeMergedsFromMatrix(maxIndex_1, maxIndex_2);

			--numOfClusters;

			addMergedToMatrix();

		} while (numOfClusters > numOfDesiredClusters);

		if (numOfClusters != numOfDesiredClusters)
		{
			System.err.println("Desired clustering number (" + numOfDesiredClusters
					+ ") is not reached(" + numOfClusters + ")!");
		}
	}

	private void mergeClustersAndSimilarities()
	{
		clusters[maxIndex_1].addAll(clusters[maxIndex_2]);
		afterMergeSimilarities = merger.mergeThese(maxIndex_1, maxIndex_2);

		Cluster mergedCluster = clusters[maxIndex_1];
		for (int i = maxIndex_1 + 1, newI = maxIndex_1; i < numOfClusters; ++i)
		{
			if (i == maxIndex_2)
			{
				continue;
			}

			clusters[newI] = clusters[i];
			afterMergeSimilarities[newI] = afterMergeSimilarities[i];
			newI++;
		}
		clusters[numOfClusters - 2] = mergedCluster;
	}

	private void removeMergedsFromMatrix(int minIndex_1, int minIndex_2)
	{
		/*
		 * On the matrix below 2nd element and 5th element is being merged.
		 * 
		 * __0_1_2_3_4_5_6 _________________________________________________
		 * 0_x_x_|_C_C_|_F _________________________________________________
		 * 1_x_x_|_C_C_|_F _________________________________________________
		 * 2-------------- _________________________________________________
		 * 3_A_A_|_D_D_|_G _________________________________________________
		 * 4_A_A_|_D_D_|_G _________________________________________________
		 * 5-------------- _________________________________________________
		 * 6_B_B_|_E_E_|_H _________________________________________________
		 * 
		 * A:(-1,0), B:(-2,0), C:(0,-1), D:(-1,-1), E:(-1,-2), F:(0,-2), G:(-1,2),
		 * H:(-2,-2)
		 */

		// A partition
		for (int i = minIndex_1; i < minIndex_2 - 1; ++i)
		{
			for (int j = 0; j < minIndex_1; ++j)
			{
				currentMatrix[i][j] = currentMatrix[i + 1][j];
			}
		}

		// B partition
		for (int i = minIndex_2; i < numOfClusters - 1; ++i)
		{
			for (int j = 0; j < minIndex_1; ++j)
			{
				currentMatrix[i - 1][j] = currentMatrix[i + 1][j];
			}
		}

		// C partition
		for (int i = 0; i < minIndex_1; ++i)
		{
			for (int j = minIndex_1; j < minIndex_2; ++j)
			{
				currentMatrix[i][j] = currentMatrix[i][j + 1];
			}
		}

		// F partition
		for (int i = 0; i < minIndex_1; ++i)
		{
			for (int j = minIndex_2; j < numOfClusters - 1; ++j)
			{
				currentMatrix[i][j - 1] = currentMatrix[i][j + 1];
			}
		}

		// D partition
		for (int i = minIndex_1; i < minIndex_2 - 1; ++i)
		{
			for (int j = minIndex_1; j < minIndex_2 - 1; ++j)
			{
				currentMatrix[i][j] = currentMatrix[i + 1][j + 1];
			}
		}

		// G partition
		for (int i = minIndex_1; i < minIndex_2 - 1; ++i)
		{
			for (int j = minIndex_2; j < numOfClusters - 1; ++j)
			{
				currentMatrix[i][j - 1] = currentMatrix[i + 1][j + 1];
			}
		}

		// E partition
		for (int i = minIndex_2; i < numOfClusters - 1; ++i)
		{
			for (int j = minIndex_1; j < minIndex_2; ++j)
			{
				currentMatrix[i - 1][j] = currentMatrix[i + 1][j + 1];
			}
		}

		// H partition
		for (int i = minIndex_2; i < numOfClusters - 1; ++i)
		{
			for (int j = minIndex_2; j < numOfClusters - 1; ++j)
			{
				currentMatrix[i - 1][j - 1] = currentMatrix[i + 1][j + 1];
			}
		}
	}

	private void addMergedToMatrix()
	{
		for (int i = 0; i < numOfClusters; ++i)
		{
			currentMatrix[numOfClusters - 1][i] = afterMergeSimilarities[i];
			currentMatrix[i][numOfClusters - 1] = afterMergeSimilarities[i];
		}
	}

	private void findMaximum()
	{
		weFoundAMaximum = false;

		double maxEdge = 0;
		for (int i = 0; i < numOfClusters; ++i)
		{
			for (int j = i + 1; j < numOfClusters; ++j)
			{
				if (currentMatrix[i][j] > maxEdge)
				{
					maxIndex_1 = i;
					maxIndex_2 = j;
					maxEdge = currentMatrix[i][j];
					weFoundAMaximum = true;
				}
			}
		}
	}

	private void generateInitialMatrix(double[][] graphMatrix)
	{
		currentMatrix = new double[numOfClusters][numOfClusters];
		for (int i = 0; i < numOfClusters; ++i)
		{
			for (int j = i + 1; j < numOfClusters; ++j)
			{
				currentMatrix[i][j] = currentMatrix[j][i] = graphMatrix[i][j];
			}
			// println("OK!! row:" + i);
		}
	}

	@SuppressWarnings("unused")
	private void printMatrix()
	{
		String separator;
		StringBuilder output = new StringBuilder("vvvv matrix vvvv\n");
		for (int i = 0; i < numOfClusters; ++i)
		{
			for (int j = 0; j < numOfClusters; ++j)
			{
				output.append(String.format("%2.1f", currentMatrix[i][j]));
				separator = " ";
				if (i == maxIndex_1)
				{
					if (j == maxIndex_2 - 1)
					{
						separator = ">";
					}
					if (j == maxIndex_2)
					{
						separator = "<";
					}
				}
				output.append(separator);
			}
			output.append("\n");
		}
		println(output.substring(0, output.length() - 1));
		println("^^^^ matrix ^^^^");
	}

	/**
	 * This class stands for formed clusters.
	 * 
	 * @author memin
	 * 
	 */
	static class Cluster extends HashSet<Integer>
	{
		private static final long serialVersionUID = 3095843345603643948L;
	}

	static void println()
	{
		if (logout)
			System.out.println();
	}

	static void print(Object output)
	{
		if (logout)
			System.out.print(output);
	}

	static void println(Object output)
	{
		if (logout)
			System.out.println(output);
	}

	public static enum Method
	{
		CL, SL, AL;
	}

	public List<BitSet> clustersAsBitSets()
	{
		List<BitSet> finalClusters = new ArrayList<BitSet>(clusters.length);
		for (int i = 0; i < numOfClusters; ++i)
		{
			Cluster cluster = clusters[i];
			BitSet clusterBitSet = new BitSet();
			for (int objectIx : cluster)
			{
				clusterBitSet.set(objectIx);
			}
			finalClusters.add(clusterBitSet);
		}
		return finalClusters;
	}

	public List<HashSet<Integer>> clusters()
	{
		List<HashSet<Integer>> finalClusters = new ArrayList<HashSet<Integer>>(clusters.length);
		for (int i = 0; i < numOfClusters; ++i)
		{
			HashSet<Integer> clusterSet = new HashSet<Integer>(clusters[i]);
			finalClusters.add(clusterSet);
		}
		return finalClusters;
	}

	double[][] getCurrentMatrix()
	{
		return currentMatrix;
	}

	public Cluster getCluster(int i)
	{
		return clusters[i];
	}
}
