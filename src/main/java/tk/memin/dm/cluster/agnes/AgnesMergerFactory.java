package tk.memin.dm.cluster.agnes;

import tk.memin.dm.cluster.agnes.Agnes.Cluster;

public class AgnesMergerFactory
{
	public static ClusterMerger singleLink()
	{
		return new SingleLinkMerger();
	}

	public static ClusterMerger completeLink()
	{
		return new CompleteLinkMerger();
	}

	public static ClusterMerger averageLink()
	{
		return new AverageLinkMerger();
	}

	private static abstract class SimpleMerger implements ClusterMerger
	{
		protected double[][] matrix;
		protected int numOfClusters;
		protected double[] afterMergeSimilarities;

		protected Agnes agnes;

		@Override
		public void setAlgorithm(Agnes agnes)
		{
			this.agnes = agnes;
			this.matrix = agnes.getCurrentMatrix();
			numOfClusters = matrix.length;
			afterMergeSimilarities = new double[numOfClusters];
		}
	}

	private static class SingleLinkMerger extends SimpleMerger
	{
		@Override
		public double[] mergeThese(int firstIndex, int secondIndex)
		{
			for (int i = 0; i < numOfClusters; ++i)
			{
				afterMergeSimilarities[i] = Math.max(matrix[i][firstIndex],
						matrix[i][secondIndex]);
			}
			return afterMergeSimilarities;
		}
	}

	private static class CompleteLinkMerger extends SimpleMerger
	{
		@Override
		public double[] mergeThese(int firstIndex, int secondIndex)
		{
			for (int i = 0; i < numOfClusters; ++i)
			{
				afterMergeSimilarities[i] = Math.min(matrix[i][firstIndex],
						matrix[i][secondIndex]);
			}
			return afterMergeSimilarities;
		}
	}

	private static class AverageLinkMerger extends SimpleMerger
	{
		/**
		 * This method does not use the secondIndex, but expects the index of merged
		 * cluster as first parameter. This is not very clean coding but it provides
		 * a minor optimisation.
		 */
		@Override
		public double[] mergeThese(int firstIndex, int secondIndex)
		{
			Cluster mergedCluster = agnes.getCluster(firstIndex);
			for (int i = 0; i < numOfClusters; ++i)
			{
				Cluster aCluster = agnes.getCluster(i);
				afterMergeSimilarities[i] = averageBetween(mergedCluster, aCluster);
			}

			return afterMergeSimilarities;
		}

		private double averageBetween(Cluster mergedCluster, Cluster aCluster)
		{
			double totalSimilarity = 0.0;

			for (int cluster_i : aCluster)
			{
				for (int cluster_j : mergedCluster)
				{
					totalSimilarity += agnes.initialSimilarityMatrix[cluster_i][cluster_j];
				}
			}

			totalSimilarity = totalSimilarity
					/ (aCluster.size() * mergedCluster.size());
			return totalSimilarity;
		}
	}
}
