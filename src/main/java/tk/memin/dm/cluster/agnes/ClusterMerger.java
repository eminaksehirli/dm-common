package tk.memin.dm.cluster.agnes;

public interface ClusterMerger
{
	double[] mergeThese(int firstIndex, int secondIndex);

	void setAlgorithm(Agnes agnes);
}
