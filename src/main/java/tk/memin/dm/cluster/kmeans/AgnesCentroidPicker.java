package tk.memin.dm.cluster.kmeans;

import java.util.HashSet;
import java.util.List;

import tk.memin.dm.cluster.agnes.Agnes;
import tk.memin.dm.cluster.agnes.AgnesMergerFactory;
import tk.memin.dm.cluster.agnes.Agnes.Method;
import tk.memin.dm.distance.ProximityCalculator;

public class AgnesCentroidPicker implements CentroidPicker
{

	private ProximityCalculator similarityCalculator;

	public AgnesCentroidPicker(ProximityCalculator similarityCalculator)
	{
		this.similarityCalculator = similarityCalculator;
	}

	@Override
	public double[][] pick(double[][] vectors, int numOfClusters)
	{
		double[][] similarityMatrix = new double[vectors.length][vectors.length];

		for (int i = 0; i < similarityMatrix.length; ++i)
		{
			for (int j = i + 1; j < similarityMatrix[0].length; ++j)
			{
				similarityMatrix[i][j] = similarityCalculator.similarityBetween(
						vectors[i], vectors[j]);
			}
		}

		Agnes agnes = new Agnes(similarityMatrix,
				AgnesMergerFactory.completeLink());
		agnes.run(numOfClusters);
		List<HashSet<Integer>> agnesClusters = agnes.clusters();

		// This can be changed with a more effective algo.
		agnesClusters = agnesClusters.subList(0, numOfClusters);

		double[][] clusterSums = new double[numOfClusters][];
		int[] clusterSizes = new int[agnesClusters.size()];
		int clusterIx = 0;
		for (HashSet<Integer> cluster : agnesClusters)
		{
			clusterSums[clusterIx] = new double[vectors[0].length];
			for (int sample : cluster)
			{
				for (int featureIx = 0; featureIx < clusterSums[clusterIx].length; ++featureIx)
				{
					clusterSums[clusterIx][featureIx] += vectors[sample][featureIx];
				}
			}
			clusterSizes[clusterIx] = cluster.size();
			clusterIx++;
		}

		for (clusterIx = 0; clusterIx < clusterSums.length; ++clusterIx)
		{
			for (int featureIx = 0; featureIx < clusterSums[clusterIx].length; ++featureIx)
			{
				clusterSums[clusterIx][featureIx] /= clusterSizes[clusterIx];
			}
		}

		return clusterSums;
	}

}
