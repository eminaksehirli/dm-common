package tk.memin.dm.cluster.kmeans;

import java.util.Random;

import tk.memin.dm.distance.EuclidianDistanceCalculator;
import tk.memin.dm.distance.ProximityCalculator;

public class KMeans
{
	public static KMeans kMeansWithAgnesCentroids(
			ProximityCalculator proximityCalculator)
	{
		KMeans kmeans = kMeansWithRandomCentroids(proximityCalculator);
		kmeans.centroidPicker = new AgnesCentroidPicker(proximityCalculator);
		return kmeans;
	}

	public static KMeans kMeansDefault()
	{
		return new KMeans();
	}

	public static KMeans kMeansWithRandomCentroids(
			ProximityCalculator proximityCalculator)
	{
		return new KMeans(proximityCalculator);
	}

	private static final int MAX_ITERATIONS = 1000;
	private Random random;
	private ProximityCalculator similarityCalculator;
	private CentroidPicker centroidPicker;

	private KMeans()
	{
		this.similarityCalculator = new EuclidianDistanceCalculator();
		this.centroidPicker = new RandomCentroidPicker();
	}

	private KMeans(ProximityCalculator proximityCalculator)
	{
		this.similarityCalculator = proximityCalculator;
		this.centroidPicker = new RandomCentroidPicker();
	}

	/**
	 * Run k-means on vectors.
	 * 
	 * @param vectors
	 *          Each line represents a vector and each columns represents a
	 *          feature.
	 * @param numOfClusters
	 *          k
	 * @return Labels of vectors ranged between <code>0</code> and
	 *         <code>numOfClusters - 1</code>;
	 */
	public int[] run(double[][] vectors, int numOfClusters)
	{
		random = new Random();

		int numOfPoints = vectors.length;
		int numOfFeatures = vectors[0].length;

		double[][] newCentroids;

		centroidPicker = new RandomCentroidPicker();
		// double[][] centroids = pickAgnesCentroids(vectors, numOfClusters);
		double[][] centroids = centroidPicker.pick(vectors, numOfClusters);
		boolean centroidsAreSame = false;

		int[] labelOf = new int[numOfPoints];
		int iterationCounter = 0;
		do
		{
			double[][] sumsOfClusters = new double[numOfClusters][numOfFeatures];
			int[] sizesOfClusters = new int[numOfClusters];

			for (int vectorIx = 0; vectorIx < vectors.length; ++vectorIx)
			{
				double[] vector = vectors[vectorIx];

				int potentialCluster = findNearestCentroid(centroids, vector);

				labelOf[vectorIx] = potentialCluster;
				++sizesOfClusters[potentialCluster];
				for (int i = 0; i < vector.length; ++i)
				{
					sumsOfClusters[potentialCluster][i] += vector[i];
				}
			}

			newCentroids = new double[numOfClusters][numOfFeatures];
			for (int i = 0; i < numOfClusters; ++i)
			{
				for (int j = 0; j < numOfFeatures; ++j)
				{
					newCentroids[i][j] = sumsOfClusters[i][j] / sizesOfClusters[i];
				}
			}

			centroidsAreSame = true;
			nextCentroid: for (int i = 0; i < numOfClusters; ++i)
			{
				for (int j = 0; j < numOfFeatures; j++)
				{
					if (centroids[i][j] - newCentroids[i][j] > 0.01)
					{
						centroidsAreSame = false;
						break nextCentroid;
					}
				}
			}

			// Check for empty clusters.

			for (int i = 0; i < sizesOfClusters.length; ++i)
			{
				if (sizesOfClusters[i] == 0)
				{
					System.err.println("We have an empty k-means cluster! Falling back to a random vector!");
					// Not the best fall back mechanism.
					double[] vector = vectors[random.nextInt(vectors.length)];
					for (int j = 0; j < vector.length; ++j)
					{
						newCentroids[i][j] = vector[j];
					}
				}
			}

			centroids = newCentroids;
			++iterationCounter;
		} while (!centroidsAreSame && iterationCounter < MAX_ITERATIONS);

		if (!centroidsAreSame)
		{
			System.err.println("K-means did not converged!");
		}

		return labelOf;

	}

	private int findNearestCentroid(double[][] centroids, double[] vector)
	{
		double maxSimilarity = -1;
		int potentialCluster = -1;
		for (int centroidIx = 0; centroidIx < centroids.length; ++centroidIx)
		{
			double[] centroid = centroids[centroidIx];
			double similarity = similarityCalculator.similarityBetween(vector,
					centroid);

			if (similarity > maxSimilarity)
			{
				maxSimilarity = similarity;
				potentialCluster = centroidIx;
			}
		}
		return potentialCluster;
	}
}
