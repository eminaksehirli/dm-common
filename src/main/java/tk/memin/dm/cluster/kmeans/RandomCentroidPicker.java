package tk.memin.dm.cluster.kmeans;

import java.util.Random;

class RandomCentroidPicker implements CentroidPicker
{
	private Random random = new Random();

	@Override
	public double[][] pick(double[][] vectors, int numOfClusters)
	{
		double[][] centroids = new double[numOfClusters][];
		boolean[] selecteds = new boolean[vectors.length];
		int randomIx;
		for (int i = 0; i < numOfClusters; ++i)
		{
			do
			{
				randomIx = random.nextInt(vectors.length);
			} while (selecteds[randomIx]);
			selecteds[randomIx] = true;

			centroids[i] = vectors[randomIx];
		}
		return centroids;
	}

}
