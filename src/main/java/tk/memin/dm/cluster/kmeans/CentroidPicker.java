package tk.memin.dm.cluster.kmeans;

public interface CentroidPicker
{

	double[][] pick(double[][] vectors, int numOfClusters);

}
