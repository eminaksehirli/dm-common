package tk.memin.dm.cluster;

/**
 * This is an implementation of Affinity Propagation method that is introduced
 * in "Clustering by Passing Messages Between Data Points" by Frey, B. and
 * Dueck, D.
 * 
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class AffinityPropagation
{
	private double[][] s; // similarities
	private double[][] r; // responsibilities
	private double[][] a; // availabilities
	private int n; // number of objects
	private int[] labels;
	private static final double Lambda = 0.5;

	public AffinityPropagation(double[][] similarityMatrix)
	{
		this.n = similarityMatrix.length;

		this.s = similarityMatrix;
		this.r = new double[n][n];
		this.a = new double[n][n];
	}

	public void runUntilStabilize()
	{
		boolean changeInTheExemplars = false;
		int[] oldExemplars = new int[n];
		do
		{
			runOneIteration();
			changeInTheExemplars = false;
			int[] exemplars = findExemplars();
			for (int i = 0; i < exemplars.length; i++)
			{
				if (oldExemplars[i] != exemplars[i])
				{
					changeInTheExemplars = true;
					break;
				}
			}
			oldExemplars = exemplars;
		} while (changeInTheExemplars);
	}

	public void runOneIteration()
	{
		labels = null;
		calculateResponsibilities();
		calculateAvailabilities();
		calculateDiagonals();
	}

	public void runTheAlgorithm(int iterationCount)
	{
		for (int i = 0; i < iterationCount; i++)
		{
			runOneIteration();
		}
	}

	/**
	 * Exemplars also stand for clusters.
	 * 
	 * @return Exemplar indices of the points.
	 */
	public int[] findExemplars()
	{
		if (labels == null)
		{
			labels = new int[n];
			for (int i = 0; i < n; i++)
			{
				double max = Integer.MIN_VALUE;
				int maxIndex = 0;
				for (int k = 0; k < n; k++)
				{
					double sum = r[i][k] + a[i][k];
					if (sum > max)
					{
						max = sum;
						maxIndex = k;
					}
				}
				labels[i] = maxIndex;
			}
		}
		return labels;
	}

	private void calculateResponsibilities()
	{
		for (int i = 0; i < n; i++)
		{
			for (int k = 0; k < n; k++)
			{
				double maxSum = 0.0;
				for (int kPrime = 0; kPrime < n; kPrime++)
				{
					// TODO: performance improvement is possible
					if (k == kPrime)
						continue;
					double sum = a[i][kPrime] + s[i][kPrime];
					if (sum > maxSum)
					{
						maxSum = sum;
					}
				}

				r[i][k] = (1 - Lambda) * (s[i][k] - maxSum) + Lambda * r[i][k];
			}
		}
	}

	private void calculateAvailabilities()
	{
		for (int i = 0; i < n; i++)
		{
			for (int k = 0; k < n; k++)
			{
				double sum = 0.0;
				for (int iPrime = 0; iPrime < n; iPrime++)
				{
					if (iPrime != i && iPrime != k)
					{
						if (r[iPrime][k] > 0)
						{
							sum += r[iPrime][k];
						}
					}
				}
				double oldA = a[i][k];
				double tempA = r[k][k] + sum;
				if (tempA < 0)
					a[i][k] = tempA;
				else
					a[i][k] = 0;

				a[i][k] = (1 - Lambda) * a[i][k] + Lambda * oldA;
			}
		}
	}

	private void calculateDiagonals()
	{
		for (int k = 0; k < n; k++)
		{
			double sum = 0.0;
			for (int iPrime = 0; iPrime < n; iPrime++)
			{
				if (r[iPrime][k] > 0)
				{
					sum += r[iPrime][k];
				}
			}
			a[k][k] = sum;
		}
	}
}
