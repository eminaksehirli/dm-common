package tk.memin.dm.cluster.evaluator;

import java.util.BitSet;

public class ICSEvaluator
{
	public static double evaluate(BitSet[] inputClusters, BitSet toEvaluate)
	{
		BitSet accumulator = new BitSet(toEvaluate.size());

		int total = 0;

		for (BitSet cluster : inputClusters)
		{
			accumulator.clear();
			accumulator.or(toEvaluate); // set

			accumulator.and(cluster);
			int size = accumulator.cardinality();

			total += size * (size - 1) / 2;
		}
		int size = toEvaluate.cardinality();
		double denominator = size * (size - 1) / 2.0;
		// double denominator = 2 * size; // TAN p.539
		if (denominator == 0)
		{
			return 0;
		}

		return (double) total / denominator;
	}

	public static double evaluateForPartition(BitSet[] inputClusters,
			BitSet[] toEvaluate)
	{
		double icsAverage = 0.0;
		for (int i = 0; i < toEvaluate.length; ++i)
		{
			icsAverage += ICSEvaluator.evaluate(inputClusters, toEvaluate[i]);
		}
		icsAverage = icsAverage / (toEvaluate.length);

		return icsAverage;
	}
}
