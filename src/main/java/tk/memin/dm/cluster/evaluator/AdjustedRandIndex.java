package tk.memin.dm.cluster.evaluator;

import java.util.Arrays;
import java.util.BitSet;

public class AdjustedRandIndex
{
	/**
	 * Calculates the Adjusted Rand Index (ARI) between two partitionings of the
	 * same data. <strong>Note:</strong> ARI <em>cannot</em> handle overlapping
	 * partitions and/or unclassified objects. Use
	 * {@link #tk.memin.dm.cluster.evaluator.NewAdjustedRandIndex.addComplement(BitSet[],int)
	 * addComplement} to add unclassified objects as a partition.
	 * 
	 * Throws {@link IllegalArgumentException} if the partitioninings do not cover
	 * the same objects, probably because there are overlapping partitions or
	 * unclassified objects.
	 * 
	 * @param partitioning_A
	 * @param partitioning_B
	 * @return Adjusted Rand Index between two partitionings.
	 * @throws IllegalArgumentException
	 */
	public static double between(BitSet[] partitioning_A, BitSet[] partitioning_B)
	{
		BitSet cover_A = getcover(partitioning_A);
		int numOfObjects = cover_A.cardinality();
		if (numOfObjects != getcover(partitioning_B).cardinality())
		{
			throw new IllegalArgumentException("Partitions are either overlapping or there are unclassified objects.");
		}

		int[][] contingencyTable = new int[partitioning_A.length][partitioning_B.length];

		int[] sumsOf_A = new int[partitioning_A.length];
		int[] sumsOf_B = new int[partitioning_B.length];

		int index = 0;

		BitSet temp = new BitSet();
		for (int i = 0; i < partitioning_A.length; i++)
		{
			BitSet cluster_1 = partitioning_A[i];
			for (int j = 0; j < partitioning_B.length; j++)
			{
				BitSet cluster_2 = partitioning_B[j];

				temp.clear();
				temp.or(cluster_1);
				temp.and(cluster_2);

				int n_ij = temp.cardinality();

				contingencyTable[i][j] = n_ij;
				sumsOf_A[i] += n_ij;
				sumsOf_B[j] += n_ij;
				index += (n_ij * (n_ij - 1)) / 2;
			}
		}

		int sumOf_sumsOf_A = 0;
		for (int a_i : sumsOf_A)
		{
			sumOf_sumsOf_A += (a_i * (a_i - 1)) / 2;
		}

		int sumOf_sumsOf_B = 0;
		for (int b_j : sumsOf_B)
		{
			sumOf_sumsOf_B += (b_j * (b_j - 1)) / 2;
		}

		double expectedIndex;

		double allCombinations = (numOfObjects * (numOfObjects - 1)) / 2;
		expectedIndex = (sumOf_sumsOf_A * sumOf_sumsOf_B) / allCombinations;

		double denominator = ((sumOf_sumsOf_A + sumOf_sumsOf_B) / 2.0)
				- expectedIndex;

		return (index - expectedIndex) / denominator;
	}

	private static BitSet getcover(final BitSet[] partitioning)
	{
		BitSet cover = new BitSet();
		for (BitSet part : partitioning)
		{
			cover.or(part);
		}
		return cover;
	}

	private static int sumOf(int[] arr)
	{
		int sum = 0;
		for (int i : arr)
		{
			sum += i;
		}
		return sum;
	}

	/**
	 * Helper method to cover partitionings with unclassified objects. It adds the
	 * objects that are not in any partitions as a new partition.
	 * 
	 * @param partitioning
	 * @param numOfObjects
	 * @return A new partitioning that has an additional partition of objects that
	 *         are not in any of the partitions.
	 */
	public static BitSet[] addComplement(BitSet[] partitioning, int numOfObjects)
	{
		BitSet complement = getComplement(partitioning, numOfObjects);
		BitSet[] extended = Arrays.copyOf(partitioning, partitioning.length + 1);
		extended[extended.length - 1] = complement;

		return extended;
	}

	private static BitSet getComplement(BitSet[] partitioning, int numOfObjects)
	{
		BitSet complement = new BitSet();
		complement.set(0, numOfObjects);
		for (BitSet cl : partitioning)
		{
			complement.xor(cl);
		}
		return complement;
	}
}
