package tk.memin.dm.cluster.evaluator;

import java.util.BitSet;

public class NMIEvaluator
{
	/**
	 * Evaluates the NMI value between two clusterings.
	 * 
	 * @param firstPartition
	 * @param secondPartition
	 * @return
	 */
	public static double evaluate(BitSet[] firstPartition,
			BitSet[] secondPartition, int numOfObjects)
	{

		int[] n_First = new int[firstPartition.length];
		int[] n_Second = new int[secondPartition.length];

		double entropyFirst = entropyFor(firstPartition, numOfObjects, n_First);
		double entropySecond = entropyFor(secondPartition, numOfObjects, n_Second);

		double numerator = 0.0;
		for (int firstIx = 0; firstIx < firstPartition.length; ++firstIx)
		{
			for (int secondIx = 0; secondIx < secondPartition.length; ++secondIx)
			{
				BitSet temp = (BitSet) firstPartition[firstIx].clone();
				temp.and(secondPartition[secondIx]);

				int n_hl = temp.cardinality();

				if (n_hl > 0)
				{
					double term = numOfObjects * n_hl
							/ (double) (n_First[firstIx] * n_Second[secondIx]);
					numerator += n_hl * Math.log(term);
//				} else
//				{
//					numerator += 0;
				}
			}
		}

		final double sqrt = Math.sqrt(entropyFirst * entropySecond);
		final double d = numerator / sqrt;
		return d;

	}

	private static double entropyFor(BitSet[] partition, int numOfObjects,
			int[] cardinalityContainer)
	{
		double entropyOfPartition = 0.0;
		for (int i = 0; i < partition.length; ++i)
		{
			cardinalityContainer[i] = partition[i].cardinality();
			if (cardinalityContainer[i] != 0)
			{
				entropyOfPartition += cardinalityContainer[i]
						* Math.log(cardinalityContainer[i] / (double) numOfObjects);
			}
		}

		return entropyOfPartition;
	}
}
