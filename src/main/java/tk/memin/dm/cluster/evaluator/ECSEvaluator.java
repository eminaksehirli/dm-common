package tk.memin.dm.cluster.evaluator;

import java.util.BitSet;

import tk.memin.dm.cluster.ensemble.ExtractClusterings.ClusterInfo;

/**
 * BEING ADAPTED FOR new algorithm (clique hunter) ****** This class is adapted
 * for ****** Electronic Letters paper experiments 12.2009 Similarity Evaluator
 * class Finds ICS+ECS of a test clustering and evaluates quality of it
 * 
 * NOTE: This class shows a sample usage of the method. You may improve the
 * design for your application.
 * 
 * @author M. Yagci - yagcim[at]gmail.com
 * @date 16.02.2010
 */

public class ECSEvaluator
{
	private int numClusterings;
	private int numObjects;
	// private int numClusters;
	private int nClusters;
	private BitSet[] clusters;

	public static double evaluateForPartition(ECSEvaluator ecs, BitSet[] partition)
	{
		double ecsAverage = 0.0;
		for (int i = 0; i < partition.length; ++i)
		{
			for (int j = i + 1; j < partition.length; ++j)
			{
				ecsAverage += ecs.evaluate(partition[i], partition[j]);
			}
		}
		// ecsAverage /= finalClusters.length;
		ecsAverage /= partition.length * (partition.length - 1) / 2.0;

		return ecsAverage;
	}

	// constructor
	public ECSEvaluator(int numOfClusterings, int numOfObjects,
			int totalNumberOfClusters, BitSet[] clusters)
	{
		try
		{
			this.numClusterings = numOfClusterings;
			this.numObjects = numOfObjects;
			this.clusters = clusters;
			this.nClusters = totalNumberOfClusters; // total no. of clusters in all
			// clusterings
		} catch (Exception e)
		{
			System.out.println(e);
		}
	}

	// constructor
	public ECSEvaluator(ClusterInfo clusterInfo)
	{
		this(clusterInfo.partitions.size(),
				clusterInfo.numOfObjects,
				clusterInfo.allClustersAsArray().length,
				clusterInfo.allClustersAsArray());
	}

	/**
	 * ------------------------------------------------------- CHANGED !!! finds
	 * overlapping ECS similarity only .... public static method to find quality
	 * of a clustering using ICS/ECS measure. Method takes a clustering as
	 * parameter.
	 */
	public double evaluate(BitSet cluster_1, BitSet cluster_2)
	{
		BitSet dummy = new BitSet(numObjects);
		double ECS;
		int count, count2, count3, count4;
		BitSet c1_OR_c2 = new BitSet(numObjects);
		BitSet c1_AND_c2 = new BitSet(numObjects);
		BitSet c1_ANDCMPL_c2 = new BitSet(numObjects);
		BitSet CMPL_c1_AND_c2 = new BitSet(numObjects);

		double sumECS = 0.0; // needed for clique algorithm
		if (cluster_1.cardinality() > 0 && cluster_2.cardinality() > 0)
		{
			c1_OR_c2.xor(c1_OR_c2); // reset
			c1_OR_c2.xor(cluster_1);
			c1_OR_c2.or(cluster_2);

			c1_AND_c2.xor(c1_AND_c2); // reset
			c1_AND_c2.xor(cluster_1);
			c1_AND_c2.and(cluster_2);

			c1_ANDCMPL_c2.xor(c1_ANDCMPL_c2); // reset
			c1_ANDCMPL_c2.xor(cluster_2);
			c1_ANDCMPL_c2.flip(0, numObjects); // complement
			c1_ANDCMPL_c2.and(cluster_1);

			CMPL_c1_AND_c2.xor(CMPL_c1_AND_c2); // reset
			CMPL_c1_AND_c2.xor(cluster_1);
			CMPL_c1_AND_c2.flip(0, numObjects); // complement
			CMPL_c1_AND_c2.and(cluster_2);

			ECS = 0.0;

			for (int i = 0; i < nClusters; i++)
			{
				dummy.xor(dummy); // reset
				dummy.or(c1_OR_c2); // clone c1ORc2
				dummy.and(clusters[i]);
				count = dummy.cardinality();

				dummy.xor(dummy); // reset
				dummy.or(c1_ANDCMPL_c2); // clone
				dummy.and(clusters[i]);
				count2 = dummy.cardinality();

				dummy.xor(dummy); // reset
				dummy.or(CMPL_c1_AND_c2); // clone
				dummy.and(clusters[i]);
				count3 = dummy.cardinality();

				// term 4 in overlapping formula
				dummy.xor(dummy); // reset
				dummy.or(c1_AND_c2); // clone c1ORc2
				dummy.and(clusters[i]);
				count4 = dummy.cardinality();

				ECS += (count * (count - 1)) / 2.0 - (count2 * (count2 - 1)) / 2.0
						- (count3 * (count3 - 1)) / 2.0 + (count4 * (count4 - 1)) / 2.0;
				// ECS += (count*(count-1))/2.0 + (count4*(count4-1))/2.0;
			}

			ECS = ECS + (c1_AND_c2.cardinality() * numClusterings); // last one is
			// for term5
			ECS /= cluster_1.cardinality() * cluster_2.cardinality();
			// quality += k2 * ECS;
			// ECS /= numClusterings; // added for normalization between 0 and 1
			sumECS += ECS;
		}
		// System.out.println(("ECS fastSIM: (ms) " +
		// (System.nanoTime()-start)/1000000.0) + "\n");

		return sumECS; // for new algorithm
	}
}