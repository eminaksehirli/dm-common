package tk.memin.dm.cluster;

import tk.memin.dm.math.Arrays;

/**
 * This class provides some tools for Affinity Propagation clustering algorithm.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class AffinityTools
{

	public static void putMedianToDiagonals(double[][] similarityMatrix)
	{
		double median = Arrays.medianOf(similarityMatrix);
		for (int i = 0; i < similarityMatrix.length; i++)
		{
			similarityMatrix[i][i] = median;
		}
	}

	public static void putMaxToDiagonals(double[][] similarityMatrix)
	{
		for (int i = 0; i < similarityMatrix.length; i++)
		{
			similarityMatrix[i][i] = 100;
		}
	}

	public static void putZeroToDiagonals(double[][] similarityMatrix)
	{
		for (int i = 0; i < similarityMatrix.length; i++)
		{
			similarityMatrix[i][i] = 0;
		}
	}
}
