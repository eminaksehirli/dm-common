package tk.memin.dm.distance;

public interface ProximityCalculatorGeneric<T>
{
	public double similarityBetween(T p1, T p2);
}
