package tk.memin.dm.distance;

public class CosineSimilarityCalculator implements ProximityCalculator
{
	@Override
	public double similarityBetween(double[] p1, double[] p2)
	{
		double divident = 0.0d;
		double firstSum = 0, secondSum = 0;

		for (int i = 0; i < p1.length; ++i)
		{
			divident += p1[i] * p2[i];
			firstSum += p1[i] * p1[i];
			secondSum += p2[i] * p2[i];
		}

		double divider = Math.sqrt(firstSum) * Math.sqrt(secondSum);

		double similarity = divident / divider;
		return similarity;
	}
}
