package tk.memin.dm.distance;

public interface ProximityCalculator extends
		ProximityCalculatorGeneric<double[]>
{
// No new methods
}