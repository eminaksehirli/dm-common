package tk.memin.dm.distance;

public class EuclidianDistanceCalculator implements ProximityCalculator
{

	public double distanceBetween(double[] p1, double[] p2)
	{
		double sum = 0.0d;
		for (int i = 0; i < p1.length; ++i)
		{
			double diff = p1[i] - p2[i];
			sum += diff * diff;
		}
		double distance = Math.sqrt(sum);
		return distance;
	}

	@Override
	public double similarityBetween(double[] p1, double[] p2)
	{
		return 1 / distanceBetween(p1, p2);
	}

}
