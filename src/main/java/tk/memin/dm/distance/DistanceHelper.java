package tk.memin.dm.distance;

import java.util.BitSet;

public final class DistanceHelper
{
	// WARNING: does not support concurrency!!
	private static BitSet dummy1 = new BitSet();
	private static BitSet dummy2 = new BitSet();

	private DistanceHelper()
	{
	}

	public static double jaccardSimilarityBetween(BitSet currentCluster,
			BitSet other)
	{
		dummy1.xor(dummy1); // will be used as numerator
		dummy2.xor(dummy2); // will be used as denominator

		dummy1.xor(currentCluster);
		dummy2.xor(currentCluster);

		dummy1.and(other);
		dummy2.or(other);
		double i = (double) dummy1.cardinality() / (double) dummy2.cardinality();

		return i;
	}

	public static int hammingBetween(BitSet currentCluster, BitSet other)
	{
		dummy1.xor(dummy1); // reset

		dummy1.xor(currentCluster);

		dummy1.xor(other);
		
		return dummy1.cardinality();
	}
}
