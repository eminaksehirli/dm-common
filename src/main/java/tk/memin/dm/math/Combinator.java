package tk.memin.dm.math;

import static java.util.Arrays.copyOf;

import java.util.Random;

/**
 * Finds the k-subsets of a set that has n elements. Returns just the indices
 * not the real elements.
 * 
 * Uses some code from <a
 * href="https://code.google.com/p/combinatoricslib/">combinatoricslib</a>
 * package.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class Combinator
{

	private int n;
	private int k;
	private int[] currentCombination;
	private int endIndex;
	private int counter;
	private long numberOfSubsets;
	private static Random random = new Random();
	private static long[][] combination_Cache;
	private static final int maxCacheSize = 29;

	/**
	 * Returns an array of subsets. Every subset is a sub-array as indices of
	 * elements.
	 * 
	 * @param n
	 *          size of superset
	 * @param k
	 *          size of subsets
	 * @return Array of indices of subsets. Indices starts from {@code 0}.
	 */
	public static int[][] k_SubsetsFor(int n, int k)
	{
		Combinator combinator = new Combinator(n, k);

		int numberOfSubsets;
		if (combinator.numberOfSubsets > Integer.MAX_VALUE)
		{
			throw new IllegalStateException("Too many subsets. Use next_k_SubsetsOf() istead!");
		}

		numberOfSubsets = (int) combinator.numberOfSubsets;

		int[][] subsets = new int[numberOfSubsets][];
		for (int i = 0; i < numberOfSubsets; ++i)
		{
			subsets[i] = copyOf(combinator.currentCombination,
					combinator.currentCombination.length);
			combinator.next();
		}

		return subsets;
	}

	/**
	 * Returns next {@code maximum} elements starting from current state.
	 * 
	 * @param combinator
	 *          combinator to get next subset from.
	 * @param maximum
	 *          maximum number of subsets to return.
	 * @return Array of indices of subsets. Indices starts from {@code 0}. If
	 *         there not {@code maximum} subsets left then the returning array's
	 *         size may be less than {@code maximum}.
	 * 
	 *         {@code null} if there are no subsets left.
	 */
	public static int[][] next_k_SubsetsOf(Combinator combinator, int maximum)
	{
		// used for controlled conversion from long to int
		long numOfSubsetsToReturnTemp = combinator.numberOfSubsets
				- combinator.counter + 1;

		int numOfSubsetsToReturn = 0;
		if (numOfSubsetsToReturnTemp > Integer.MAX_VALUE)
		{
			numOfSubsetsToReturn = Integer.MAX_VALUE;
		}

		if (numOfSubsetsToReturn <= 0)
		{
			return null;
		}
		if (numOfSubsetsToReturn > maximum)
		{
			numOfSubsetsToReturn = maximum;
		}

		int[][] subsets = new int[numOfSubsetsToReturn][];
		int i = 0;

		for (i = 0; i < numOfSubsetsToReturn; ++i)
		{
			subsets[i] = copyOf(combinator.currentCombination,
					combinator.currentCombination.length);
			combinator.next();
		}

		return subsets;
	}

	/**
	 * Selects {@code count} random k-subsets from the subsets of an {@code n}
	 * -element set. Selection is done randomly, so there is a huge possibility of
	 * clashes in low dimensional data sets. Use {@link random_k_SubsetsFor} for
	 * low dimensional data sets.
	 * 
	 * @param n
	 *          Cardinality of superset.
	 * @param k
	 *          Cardinality of subsets.
	 * @param numberOfSets
	 *          number of subsets to produce.
	 * @return Subsets.
	 */
	public static int[][] random_k_NonUniqueSubsets(int n, int k, int numberOfSets)
	{
		boolean[] selectedElements = new boolean[n];

		int[][] subsets = new int[numberOfSets][];
		int r, length;

		for (int i = 0; i < numberOfSets; ++i)
		{
			int[] subset = new int[k];
			length = 0;

			do
			{
				r = random.nextInt(n);
				if (!selectedElements[r])
				{
					subset[length] = r;
					++length;
					selectedElements[r] = true;
				}
			} while (length < k);

			subsets[i] = subset;

			// Leave the array as found
			for (int index : subset)
			{
				selectedElements[index] = false;
			}
		}

		return subsets;
	}

	/**
	 * Calculates value of C(n,k) where C means combination. This value also gives
	 * the number of k-subsets of set with n elements. Calculated value is
	 * 
	 * n! / (k! * (n-k)!)
	 * 
	 * @param n
	 *          Size of set to choose from.
	 * @param k
	 *          Element count of subsets
	 * @return Number of k-subsets of a set with n elements.
	 */
	public static long calculate(int n, int k)
	{
		long numerator = 1;
		long denumerator = 1;
		for (int i = 0; i < k; ++i)
		{
			numerator = numerator * (n - i);
			denumerator = denumerator * (i + 1);
		}

		long numberOfSubsets = numerator / denumerator;
		return numberOfSubsets;
	}

	/**
	 * Same as {@link Combinator.calculate} but returns values from a look-up
	 * table. Table is generated at first call of the function.
	 * 
	 * WARNING: This is not a clean implementation: May cause instability!
	 * 
	 * @param n
	 *          Size of set to choose from.
	 * @param k
	 *          Element count of subsets
	 * @return Number of k-subsets of a set with n elements.
	 */
	public static long calculateWithCache(int n, int k)
	{
		if (combination_Cache == null)
		{
			long[][] new_CombCache = new long[maxCacheSize][];
			for (int top = 2; top <= maxCacheSize; ++top)
			{
				new_CombCache[top - 1] = new long[top];
				int bottomMax = (top + 1) / 2;
				for (int bottom = 1; bottom <= bottomMax; ++bottom)
				{
					long numerator = 1;
					long denumerator = 1;
					for (int i = 0; i < bottom; ++i)
					{
						numerator = numerator * (top - i);
						denumerator = denumerator * (i + 1);
					}
					new_CombCache[top - 1][bottom - 1] = numerator / denumerator;
					new_CombCache[top - 1][top - bottom - 1] = new_CombCache[top - 1][bottom - 1];
				}
			}
			combination_Cache = new_CombCache;
		}
		try
		{
			return combination_Cache[n - 1][k - 1];
		} catch (ArrayIndexOutOfBoundsException e)
		{
			return calculate(n, k);
		}
	}

	public Combinator(int n, int k)
	{
		this.n = n;
		this.k = k;
		currentCombination = new int[k];
		endIndex = 1;
		counter = 1;
		numberOfSubsets = calculateWithCache(n, k);

		for (int i = 0; i < k; ++i)
		{
			currentCombination[i] = i;
		}
	}

	/**
	 * Calculates the next combination.
	 */
	public void next()
	{
		counter++;
		endIndex = k - 1;

		while (currentCombination[endIndex] == n - k + endIndex)
		{
			endIndex--;
			if (endIndex == 0)
				break;
		}

		++currentCombination[endIndex];
		for (int i = endIndex + 1; i < k; ++i)
		{
			currentCombination[i] = currentCombination[i - 1] + 1;
		}
	}

	public boolean hasNext()
	{
		return counter != numberOfSubsets;
	}

	public int[] currentCombination()
	{
		return currentCombination;
	}
}
