package tk.memin.dm.math;

import static java.lang.Integer.MAX_VALUE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Arrays
{
	/**
	 * Finds the maximum value in the target array.
	 * 
	 * @param target
	 * @return Maximum value in target.
	 */
	public static int max(int[] target)
	{
		int max = 0;
		for (int i : target)
		{
			if (i > max)
			{
				max = i;
			}
		}
		return max;
	}

	/**
	 * Finds the maximum value in the target array.
	 * 
	 * @param target
	 * @return Maximum value in target.
	 */
	public static double max(double[] target)
	{
		double max = 0;
		for (double i : target)
		{
			if (i > max)
			{
				max = i;
			}
		}
		return max;
	}

	/**
	 * Finds the minimum value in the target array.
	 * 
	 * @param target
	 * @return Minimum value in target.
	 */
	public static double min(double[] target)
	{
		double min = MAX_VALUE;
		for (double i : target)
		{
			if (i < min)
			{
				min = i;
			}
		}
		return min;
	}

	/**
	 * Finds the median of a similarity matrix. It assumes the matrix is symmetric
	 * and it ignores the values on the diagonal.
	 * 
	 * @param similarityMatrix
	 *          A symmetric square matrix.
	 * @return Median of the values in the upper right triangle of the matrix,
	 *         excluding the diagonal.
	 */
	public static double medianOf(double[][] similarityMatrix)
	{
		List<Double> allSimilarities = new ArrayList<Double>((similarityMatrix.length * (similarityMatrix.length + 1)) / 2);
		for (int i = 0; i < similarityMatrix.length; i++)
		{
			for (int j = i + 1; j < similarityMatrix.length; j++)
			{
				allSimilarities.add(similarityMatrix[i][j]);
			}
		}
		Collections.sort(allSimilarities);
		double median = allSimilarities.get(allSimilarities.size() / 2);
		return median;
	}
}
