package tk.memin.dm.math;

public class Normalize
{
	private Normalize()
	{
	}

	/**
	 * Normalizes values using the min-max normalization method.
	 * 
	 * @param values
	 *          Values to be normalized. Normalization takes in place. So the
	 *          content of array will be changed.
	 */
	public static void usingMinMaxMethod(double[] values)
	{
		double min = values[0], max = values[0];
		for (double value : values)
		{
			if (value > max)
			{
				max = value;
			}
			if (value < min)
			{
				min = value;
			}
		}

		double divider = max - min;
		for (int i = 0; i < values.length; ++i)
		{
			values[i] = values[i] / divider;
		}
	}

	/**
	 * Normalizes values using the Z-Score normalization method.
	 * 
	 * @param values
	 *          Values to be normalized. Normalization takes in place. So the
	 *          content of array will be changed.
	 */
	public static void usingZScoreMethod(double[] values)
	{
		double total = 0;
		for (double value : values)
		{
			total += value;
		}

		double average = total / values.length;
		double totalError = 0.0;

		for (double value : values)
		{
			totalError += (average - value) * (average - value);
		}

		double standartDeviation = Math.sqrt(totalError / values.length);

		for (int i = 0; i < values.length; ++i)
		{
			values[i] = values[i] - average / standartDeviation;
		}
	}
}
