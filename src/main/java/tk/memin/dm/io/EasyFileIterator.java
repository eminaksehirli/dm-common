package tk.memin.dm.io;

import java.util.Iterator;

public class EasyFileIterator implements Iterator<String>
{
	private EasyFileReader reader;
	private String nextLine;

	public EasyFileIterator(EasyFileReader reader)
	{
		this.reader = reader;
		nextLine = reader.readALine();
	}

	@Override
	public boolean hasNext()
	{
		return nextLine != null;
	}

	@Override
	public String next()
	{
		String currentLine = nextLine;
		if (currentLine != null)
		{
			nextLine = reader.readALine();
		}
		return currentLine;
	}

	@Override
	public void remove()
	{
		throw new UnsupportedOperationException();
	}
}
