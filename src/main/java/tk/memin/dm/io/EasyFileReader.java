package tk.memin.dm.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

public class EasyFileReader implements Iterable<String>
{
	private BufferedReader reader;

	/**
	 * Returns null if EOL is reached.
	 */
	public String readALine()
	{
		String line = null;
		try
		{
			line = reader.readLine();
		} catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
		return line;
	}

	public EasyFileReader(String fileName)
	{
		File file = new File(fileName);
		try
		{
			Reader in = new FileReader(file);
			reader = new BufferedReader(in);
		} catch (FileNotFoundException e)
		{
			throw new IllegalStateException(e);
		}
	}

	@Override
	public Iterator<String> iterator()
	{
		return new EasyFileIterator(this);
	}

	public void close()
	{
		try
		{
			reader.close();
		} catch (IOException e)
		{
			throw new IllegalStateException(e);
		}
	}
}
