package tk.memin.dm.matrix;

import static java.awt.Color.black;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class MatrixPane extends JPanel
{
	private static final long serialVersionUID = 6585392843171261529L;
	private static final int Y_Ofset = 10;
	private static final int X_Ofset = 10;
	private int cellHeight;
	private int cellWidth;
	private int[][] matrix;

	public MatrixPane(int[][] matrix)
	{
		this.matrix = matrix;

		setMinimumSize(new Dimension(matrix.length, matrix[0].length));
		cellWidth = 10;
		cellHeight = 10;
	}

	@Override
	public Dimension getPreferredSize()
	{
		return new Dimension(matrix[0].length, matrix.length);
	}

	@Override
	public void paintComponent(Graphics g_1)
	{
		Graphics2D g = (Graphics2D) g_1;
		super.paintComponent(g);
		
		
		setCellSize();

		for (int i = 0; i < matrix[0].length; i += 100)
		{
			g.setColor(black);
			g.drawLine(i * cellWidth + X_Ofset, 0, i * cellWidth + X_Ofset, Y_Ofset);
		}

		for (int i = 0; i < matrix.length; i += 100)
		{
			g.setColor(black);
			g.drawLine(0, i * cellHeight + Y_Ofset, X_Ofset, i * cellHeight + Y_Ofset);
		}

		int y = Y_Ofset;
		for (int[] row : matrix)
		{
			int x = X_Ofset;
			for (int cell : row)
			{
				g.setColor(new Color(cell, cell, cell));
				g.fillRect(x, y, cellWidth, cellHeight);
				x += cellWidth;
			}
			y += cellHeight;
		}
	}

	private void setCellSize()
	{
		int width = this.getWidth();
		int height = this.getHeight();

		cellHeight = Math.max(1, height / matrix.length);
		cellWidth = Math.max(1, width / matrix[0].length);
	}
}
