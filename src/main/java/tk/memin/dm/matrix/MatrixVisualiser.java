package tk.memin.dm.matrix;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static java.lang.Integer.MAX_VALUE;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;

import tk.memin.dm.collection.TopKElements;

/**
 * Provides methods to visualise a matrix.
 * 
 * @author M. Emin Aksehirli
 * 
 */
public class MatrixVisualiser
{
	private static final String End_Matrix = "^^^^ matrix ^^^^";
	private static final String Start_Matrix = "vvvv matrix vvvv\n";

	/**
	 * Prints the matrix to the standart output and marks the top-k values of each
	 * row.
	 * 
	 * @param matrix
	 * @param topK
	 */
	public static void printRowMaxes(double[][] matrix, int topK)
	{
		StringBuilder output = new StringBuilder(Start_Matrix);
		for (int i = 0; i < matrix.length; ++i)
		{
			StringBuilder rowOut = new StringBuilder();
			double[] row = matrix[i];

			Set<Integer> rowMaxIxs = new HashSet<Integer>(TopKElements.of(row, topK));

			for (int j = 0; j < matrix[0].length; ++j)
			{
				rowOut.append(String.format("%5.2f", row[j]));

				String separator = " ";
				if (rowMaxIxs.contains(j + 1))
				{
					separator = ">";
				} else if (rowMaxIxs.contains(j))
				{
					separator = "<";
				}

				rowOut.append(separator);
			}
			output.append(rowOut).append("\n");
		}

		System.out.println(output.substring(0, output.length() - 1));
		System.out.println(End_Matrix);
	}

	/**
	 * Prints a graphical representation of a matrix. Thresholds for 4 different
	 * intensity values are the average, average + StDev/2, and average + StDev/4.
	 * 
	 * @param matrix
	 */
	public static void printImageSD(int[][] matrix)
	{
		printImageSD(new NumericMatrix(matrix));
	}

	/**
	 * Prints a graphical representation of a matrix. Thresholds for 4 different
	 * intensity values are the average, average + StDev/2, and average + StDev/4.
	 * 
	 * @param matrix
	 */
	public static void printImageSD(double[][] matrix)
	{
		printImageSD(new NumericMatrix(matrix));
	}

	/**
	 * Prints a graphical representation of a matrix. Thresholds for 4 different
	 * intensity values are the largest 3 increases (leaps) in the matrix.
	 * 
	 * @param matrix
	 */
	public static void printImageLeaps(int[][] matrix)
	{
		printImageLeaps(new NumericMatrix(matrix));
	}

	/**
	 * Prints a graphical representation of a matrix. Thresholds for 4 different
	 * intensity values are the largest 3 increases (leaps) in the matrix.
	 * 
	 * @param matrix
	 */
	public static void printImageLeaps(double[][] matrix)
	{
		printImageLeaps(new NumericMatrix(matrix));
	}

	/**
	 * Prints a graphical representation of a matrix. Thresholds for 4 different
	 * intensity values are the median, tri-octile (5/8), and quantile (3/4).
	 * 
	 * @param matrix
	 */
	public static void printImageMedian(int[][] matrix)
	{
		printImageMedian(new NumericMatrix(matrix));
	}

	/**
	 * Prints a graphical representation of a matrix. Thresholds for 4 different
	 * intensity values are the median, tri-octile (5/8), and quantile (3/4).
	 * 
	 * @param matrix
	 */
	public static void printImageMedian(double[][] matrix)
	{
		printImageMedian(new NumericMatrix(matrix));
	}

	/**
	 * Shows the matrix as a gray-scaled image where intensity values are the cell
	 * values.
	 * 
	 * @param matrix
	 */
	public static void showFrame(double[][] matrix)
	{
		final String title = "Matrix";
		showFrame(matrix, title);
	}

	/**
	 * Shows the matrix as a gray-scaled image where intensity values are the cell
	 * values.
	 * 
	 * @param matrix
	 * @return
	 */

	public static JFrame showFrame(int[][] matrix)
	{
		final String title = "Matrix";
		return showFrame(matrix, title);
	}

	/**
	 * Shows the matrix as a gray-scaled image where intensity values are the cell
	 * values.
	 * 
	 * @param matrix
	 * @param title
	 *          Title of the window
	 * @return
	 * 
	 */

	public static JFrame showFrame(double[][] matrix, final String title)
	{
		int[][] processedMatrix = grayScale(new NumericMatrix(matrix));
		return showFrame2(processedMatrix, title);
	}

	/**
	 * Shows the matrix as a gray-scaled image where intensity values are the cell
	 * values.
	 * 
	 * @param matrix
	 * @param title
	 *          Title of the window
	 * @return
	 * 
	 */

	public static JFrame showFrame(int[][] matrix, final String title)
	{
		int[][] processedMatrix = grayScale(matrix);
		return showFrame2(processedMatrix, title);
	}

	public static int[][] grayScale(int[][] matrix)
	{
		int[][] processedMatrix = grayScale(new NumericMatrix(matrix));
		return processedMatrix;
	}

	public static JScrollPane getPanel(int[][] matrix)
	{
		int[][] processedMatrix = grayScale(matrix);
		final JScrollPane sPane = wrapToScroll(new MatrixPane(processedMatrix));
		return sPane;
	}

	private static void printImageSD(final NumericMatrix matrix)
	{
		double[] allScores = flatten(matrix);

		double total = 0;
		for (double i : allScores)
		{
			total += i;
		}

		double average = total / allScores.length;

		double sse = 0;
		for (double i : allScores)
		{
			final double d = average - i;
			sse += d * d;
		}
		double sd = Math.sqrt(sse);

		print(matrix, average, average + sd / 2, average + sd / 4);
	}

	private static void printImageLeaps(final NumericMatrix matrix)
	{
		double[] allScores = flatten(matrix);
		Arrays.sort(allScores);

		double[][] diffs = new double[allScores.length - 1][];

		for (int i = 0; i < allScores.length - 1; i++)
		{
			diffs[i] = new double[]
			{ allScores[i + 1] - allScores[i], i + 1 };
		}

		Arrays.sort(diffs, new Comparator<double[]>() {
			@Override
			public int compare(double[] o1, double[] o2)
			{
				return Double.compare(o2[0], o1[0]);
			}
		});

		double high = diffs[0][1], med = diffs[0][1], low = diffs[0][1];

		if (diffs[0][1] > diffs[1][1])
		{
			if (diffs[2][1] > diffs[1][1])
			{
				low = diffs[1][1];
				if (diffs[2][1] < diffs[0][1])
				{
					med = diffs[2][1];
				} else
				{
					med = diffs[1][1];
				}
			} else
			{
				low = diffs[2][1];
				med = diffs[1][1];
			}
		} else
		{
			if (diffs[2][1] > diffs[1][1])
			{
				low = diffs[0][1];
				med = diffs[1][1];
			} else
			{
				if (diffs[0][1] < diffs[2][1])
				{
					low = diffs[0][1];
					med = diffs[2][1];
				} else
				{
					med = diffs[1][1];
					low = diffs[2][1];
				}
			}
		}

		print(matrix, allScores[(int) low], allScores[(int) med],
				allScores[(int) high]);
	}

	private static void printImageMedian(NumericMatrix matrix)
	{
		double[] allScores = flatten(matrix);

		Arrays.sort(allScores);
		double median = allScores[(allScores.length / 2) - 1];
		double triOctile = allScores[((5 * allScores.length) / 8) - 1];
		double quantile = allScores[((3 * allScores.length) / 4) - 1];

		print(matrix, median, triOctile, quantile);
	}

	private static JFrame showFrame2(int[][] processedMatrix, String t)
	{
		final String title = t + " (" + processedMatrix.length + "x"
				+ processedMatrix[0].length + ")";
		JFrame frame = new JFrame(title);
		frame.setLayout(new BorderLayout(10, 10));

		final MatrixPane mPane = new MatrixPane(processedMatrix);
		final JScrollPane sPane = wrapToScroll(mPane);
		frame.add(sPane, BorderLayout.CENTER);

		final JTextField fileName = new JTextField("/tmp/" + t + ".png");
		final JButton save = new JButton("Save");

		final JPanel bottomPane = new JPanel();
		bottomPane.setLayout(new BorderLayout());

		bottomPane.add(fileName, BorderLayout.WEST);
		bottomPane.add(save, BorderLayout.EAST);

		frame.add(bottomPane, BorderLayout.SOUTH);

		final int height = processedMatrix.length;
		final int width = processedMatrix[0].length;
		final ActionListener action = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				BufferedImage img = new BufferedImage(mPane.getHeight(),
						mPane.getWidth(),
						TYPE_INT_RGB);
				mPane.paintComponent(img.createGraphics());

				writeImage(img, fileName.getText());
			}
		};
		save.addActionListener(action);
		fileName.addActionListener(action);

		int horSize = Math.min(width + 15, 900);
		int vertSize = Math.min(height + 80, 900);

		frame.setSize(horSize, vertSize);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		return frame;
	}

	private static JScrollPane wrapToScroll(final MatrixPane mPane)
	{
		final JScrollPane sPane = new JScrollPane(mPane);
		sPane.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
		return sPane;
	}

	private static int[][] grayScale(NumericMatrix matrix)
	{
		double max = 0;
		double min = MAX_VALUE;

		for (int rowIx = 0; rowIx < matrix.length(); rowIx++)
		{
			for (int colIx = 0; colIx < matrix.length(rowIx); colIx++)
			{
				double cell = matrix._(rowIx, colIx);
				if (max < cell)
				{
					max = cell;
				}
				if (min > cell)
				{
					min = cell;
				}
			}
		}

		double stepSize = (max - min) / 255;

		int[][] processedMatrix = new int[matrix.length()][matrix.length(0)];

		for (int rowIx = 0; rowIx < matrix.length(); rowIx++)
		{
			for (int cellIx = 0; cellIx < matrix.length(rowIx); cellIx++)
			{
				processedMatrix[rowIx][cellIx] = (int) ((matrix._(rowIx, cellIx) - min) / stepSize);
			}
		}
		return processedMatrix;
	}

	/**
	 * Prints a graphical representation of a matrix. Thresholds for intensities
	 * of the cells are given as parameters.
	 * 
	 * @param matrix
	 *          Matrix to be printed.
	 * @param low
	 *          Values greater than this value are printed as lightest ( ░ ).
	 *          Values less than or equal will not be printed.
	 * @param medium
	 *          Values greater than this value are printed with a medium intensity
	 *          ( ▓ ).
	 * @param top
	 *          Values greater than this value are printed as darkest ( █ ).
	 */
	private static void print(NumericMatrix matrix, double low, double medium,
			double top)
	{
		for (int i = 0; i < matrix.length(); i++)
		{
			for (int j = 0; j < matrix.length(i); j++)
			{
				if (matrix._(i, j) > top)
				{
					System.out.print("█");
				} else if (matrix._(i, j) > medium)
				{
					System.out.print("▓");
				} else if (matrix._(i, j) > low)
				{
					System.out.print("░");
				} else
				{
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

	private static double[] flatten(NumericMatrix matrix)
	{
		double[] allScores = new double[matrix.length() * matrix.length(0)];
		int counter = 0;
		for (int i = 0; i < matrix.length(); i++)
		{
			for (int j = 0; j < matrix.length(i); j++)
			{
				allScores[counter++] = matrix._(i, j);
			}
		}
		return allScores;
	}

	public static void writeImage(BufferedImage img, final String file)
	{
		try
		{
			if (ImageIO.write(img, "png", new File(file)))
			{
				System.out.println("-- saved as " + file);
			}
		} catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}

	private static class NumericMatrix
	{
		private double[][] matrix;
		private int[][] matrixI;

		public NumericMatrix(double[][] matrix)
		{
			this.matrix = matrix;
		}

		public NumericMatrix(int[][] matrix)
		{
			this.matrixI = matrix;
		}

		double _(int i, int j)
		{
			if (matrix != null)
				return matrix[i][j];
			return matrixI[i][j];
		}

		int length(int rowIx)
		{
			if (matrix != null)
				return matrix[rowIx].length;
			return matrixI[rowIx].length;
		}

		int length()
		{
			if (matrix != null)
				return matrix.length;
			return matrixI.length;
		}
	}

}
